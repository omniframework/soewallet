﻿using Generic.Core.Services.Impl;
using Security.Data.Repositories;
using Security.Entities.DataTransfers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Core.Impl
{
    public class ServiceUsuario : ServiceBase<IRepoUsuario>, IServiceUsuario
    {
        public ServiceUsuario(IRepoUsuario mainRepo) : base(mainRepo)
        {
        }

        public UsuarioDto Get(long usuarioId)
        {
            return this._mainRepo.Get(usuarioId);
        }
    }
}
