﻿using Generic.Core.Services.Impl;
using Generic.DataTransfer.Enums;
using Security.Core.Resources;
using Security.Data.Repositories;
using Security.Entities.DataTransfers;
using Security.Entities.Params;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Core.Impl
{
    public class ServiceSession : ServiceBase<IRepoUsuario>, IServiceSession
    {
        public ServiceSession(IRepoUsuario mainRepo) : base(mainRepo)
        {
        }

        public UsuarioDto ChangePassword(ChangePasswordParam param)
        {            
            #region Validaciones
            ProcessValidations(
                (messages) =>
                {
                    UsuarioDto usuario = this._mainRepo.Get(param.UsuarioId);
                    if (usuario == null)
                    {
                        messages.Add(string.Format(ServiceSessionMessages.UserByIdNoExist, param.UsuarioId));
                    }
                    else if (usuario.Estado == StateEnum.Disabled)
                    {
                        messages.Add(ServiceSessionMessages.UserBloqued);
                    }

                    usuario = this._mainRepo.GetBy(param.UsuarioId, param.Password);
                    if (usuario == null)
                    {
                        messages.Add(ServiceSessionMessages.UserPasswordInvalid);
                    }
                });
            #endregion
            this._mainRepo.ChangePassword(param);
            this._mainRepo.SaveChanges();
            return this._mainRepo.Get(param.UsuarioId);
        }

        public UsuarioDto Login(LoginParam param)
        {            
            UsuarioDto usuario = this._mainRepo.GetBy(param);
            #region Validaciones
            ProcessValidations(
                (messages) =>
                {
                    if (usuario == null)
                    {
                        messages.Add(ServiceSessionMessages.NickNamePasswordInvalid);
                    }
                    else if (usuario.Estado == StateEnum.Disabled)
                    {
                        messages.Add(ServiceSessionMessages.UserBloqued);
                    }
                });
            #endregion
            return usuario;
        }
    }
}
