﻿using Generic.Core.Services;
using Security.Entities.DataTransfers;
using Security.Entities.Params;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Core
{
    public interface IServiceSession : IServiceBase
    {
        UsuarioDto Login(LoginParam param);

        UsuarioDto ChangePassword(ChangePasswordParam param);
    }
}
