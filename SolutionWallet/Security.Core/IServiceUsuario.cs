﻿using Generic.Core.Services;
using Security.Entities.DataTransfers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Core
{
    public interface IServiceUsuario : IServiceBase
    {
        UsuarioDto Get(long usuarioId);
    }
}
