﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.Enums
{
    public static class TransaccionTipoEnum
    {
        public const short TransferenciaIngreso = 3;
        public const short TransferenciaEgreso = 4;
    }
}
