﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataViews
{
    public class PostTransferenciaParam
    {
        public long CuentaOrigenId { get; set; }
        public long CuentaDestinoId { get; set; }
        public decimal Monto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }        
        public short TransferenciaTipoId { get; set; }        
    }
}
