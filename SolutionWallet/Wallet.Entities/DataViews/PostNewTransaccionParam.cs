﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataViews
{
    public class PostNewTransaccionParam
    {
        public short TransaccionTipoId { get; set; }
        public long CuentaId { get; set; }
        public short MonedaId { get; set; }
        public decimal MontoOriginal { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
    }
}
