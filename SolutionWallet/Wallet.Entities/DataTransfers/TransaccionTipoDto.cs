﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class TransaccionTipoDto
    {
        public short TransaccionTipoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public bool Ingreso { get; set; }
        public bool Editable { get; set; }
        public bool Seleccionable { get; set; }
    }
}
