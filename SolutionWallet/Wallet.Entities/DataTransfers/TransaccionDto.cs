﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class TransaccionDto
    {
        public long TransaccionId { get; set; }
        public DateTime FechaOcurrencia { get; set; }
        public short TransaccionTipoId { get; set; }
        public long CuentaId { get; set; }
        public short MonedaOriginalId { get; set; }
        public short MonedaCuentaId { get; set; }
        public decimal TipoCambioValor { get; set; }
        public decimal MontoOriginal { get; set; }
        public decimal Monto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string MotivoAnulacion { get; set; }
        public DateTime? FechaAnulacion { get; set; }
        public short Estado { get; set; }

        public string MonedaOrigenAbreb { get; set; }
        public string MonedaOrigenName { get; set; }
        public string MonedaCuentaAbreb { get; set; }
        public string MonedaCuentaName { get; set; }
        public string TransaccionTipo { get; set; }
    }
}
