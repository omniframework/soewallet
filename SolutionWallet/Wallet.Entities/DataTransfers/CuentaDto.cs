﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class CuentaDto
    {
        public long CuentaId { get; set; }
        public long UsuarioId { get; set; }
        public decimal Balance { get; set; }
        public short CuentaTipoId { get; set; }
        public short MonedaId { get; set; }
        public short Estado { get; set; }
        public string CuentaTipoName { get; set; }
        public string MonedaAbreb { get; set; }
        public string MonedaName { get; set; }
    }
}
