﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class TransferenciaTipoDto
    {
        public short TransferenciaTipoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
    }
}
