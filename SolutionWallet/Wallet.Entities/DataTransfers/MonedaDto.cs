﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class MonedaDto
    {
        public short MonedaId { get; set; }
        public string Abreviacion { get; set; }
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
    }
}
