﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Entities.DataTransfers
{
    public class TransferenciaDto
    {
        public long TransferenciaId { get; set; }
        public long TransaccionOrigenId { get; set; }
        public long TransaccionDestinoId { get; set; }
        public long CuentaOrigenId { get; set; }
        public long CuentaDestinoId { get; set; }
        public DateTime FechaOcurrencia { get; set; }
        public decimal MontoOriginal { get; set; }
        public short MonedaOrigenId { get; set; }
        public short MonedaDestinoId { get; set; }
        public decimal TipoCambioValor { get; set; }
        public decimal Monto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public DateTime? FechaAnulacion { get; set; }
        public string MotivoAnulacion { get; set; }
        public short TransferenciaTipoId { get; set; }        
        public string MonedaOrigenAbreb { get; set; }
        public string MonedaOrigenName { get; set; }
        public string MonedaDestinoAbreb { get; set; }
        public string MonedaDestinoName { get; set; }
        public string TransferenciaTipo { get; set; }
    }
}
