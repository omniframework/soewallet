﻿using Generic.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.UnitOfWork
{
    public interface IWalletUnitOfWork : IUnitOfWorkBase
    {
        DbSet<Cuenta> Cuenta { get; set; }
        DbSet<CuentaAsociada> CuentaAsociada { get; set; }
        DbSet<CuentaTipo> CuentaTipo { get; set; }
        DbSet<Moneda> Moneda { get; set; }
        DbSet<TipoCambio> TipoCambio { get; set; }
        DbSet<Transaccion> Transaccion { get; set; }
        DbSet<TransaccionTipo> TransaccionTipo { get; set; }
        DbSet<Transferencia> Transferencia { get; set; }
        DbSet<TransferenciaTipo> TransferenciaTipo { get; set; }
        DbSet<UsuarioBilletera> UsuarioBilletera { get; set; }
    }
}
