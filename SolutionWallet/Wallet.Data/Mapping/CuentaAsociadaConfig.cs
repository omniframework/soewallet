﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class CuentaAsociadaConfig : IEntityTypeConfiguration<CuentaAsociada>
    {
        public void Configure(EntityTypeBuilder<CuentaAsociada> builder)
        {
            builder
                .HasKey(c => c.CuentaAsociadaId);

            builder
                .Property(c => c.CuentaAsociadaId)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.UsuarioId)
                .IsRequired(true);

            builder.Property(c => c.Alias)
                .HasMaxLength(200)
                .IsRequired(true);

            builder.Property(c => c.CuentaId)
                .IsRequired(true);

            builder.HasOne(c => c.Cuenta)
                .WithMany(m => m.CuentaAsociadas)
                .HasForeignKey(c => c.CuentaId);

            builder.HasOne(c => c.UsuarioBilletera)
                .WithMany(m => m.CuentaAsociadas)
                .HasForeignKey(c => c.UsuarioId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
