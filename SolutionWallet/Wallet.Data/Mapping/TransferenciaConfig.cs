﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class TransferenciaConfig : IEntityTypeConfiguration<Transferencia>
    {
        public void Configure(EntityTypeBuilder<Transferencia> builder)
        {
            builder
                .HasKey(c => c.TransferenciaId);

            builder.Property(c => c.TransferenciaId)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.MontoOriginal)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.Monto)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.TipoCambioValor)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.MotivoAnulacion)
                .HasMaxLength(250)
                .IsRequired(false);

            builder.Property(c => c.FechaAnulacion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaOcurrencia)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.MonedaDestinoId)
                .IsRequired(true);

            builder.Property(c => c.MonedaOrigenId)
                .IsRequired(true);

            builder.Property(c => c.TransaccionDestinoId)
                .IsRequired(true);

            builder.Property(c => c.TransaccionOrigenId)
                .IsRequired(true);

            builder.Property(c => c.TransferenciaTipoId)
                .IsRequired(true);

            builder.Property(c => c.CuentaOrigenId)
                .IsRequired(true);

            builder.Property(c => c.CuentaDestinoId)
                .IsRequired(true);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.HasOne(c => c.MonedaOrigen)
                .WithMany(m => m.TransferenciaEnviadas)
                .HasForeignKey(c => c.MonedaOrigenId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.MonedaDestino)
                .WithMany(m => m.TransferenciaRecividas)
                .HasForeignKey(c => c.MonedaDestinoId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.TransferenciaTipo)
                .WithMany(m => m.Transferencias)
                .HasForeignKey(c => c.TransferenciaTipoId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.TransaccionOrigen)
                .WithMany(m => m.TransferenciasOrigen)
                .HasForeignKey(c => c.TransaccionOrigenId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.TransaccionDestino)
                .WithMany(m => m.TransferenciasDestino)
                .HasForeignKey(c => c.TransaccionDestinoId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.CuentaOrigen)
                .WithMany(m => m.TransferenciaEnviadas)
                .HasForeignKey(c => c.CuentaOrigenId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.CuentaDestino)
                .WithMany(m => m.TransferenciaRecividas)
                .HasForeignKey(c => c.CuentaDestinoId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
