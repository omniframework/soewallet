﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class TransferenciaTipoConfig : IEntityTypeConfiguration<TransferenciaTipo>
    {
        public void Configure(EntityTypeBuilder<TransferenciaTipo> builder)
        {
            builder
                .HasKey(c => c.TransferenciaTipoId);

            builder
                .Property(c => c.TransferenciaTipoId)
                .ValueGeneratedNever();

            builder.Property(c => c.Nombre)
                .HasMaxLength(100)
                .IsRequired(true);

            builder.Property(c => c.Descripcion)
                .HasMaxLength(255)
                .IsRequired(false);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);
        }
    }
}
