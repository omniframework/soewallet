﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class TransaccionConfig : IEntityTypeConfiguration<Transaccion>
    {
        public void Configure(EntityTypeBuilder<Transaccion> builder)
        {
            builder
                .HasKey(c => c.TransaccionId);

            builder.Property(c => c.TransaccionId)
                .ValueGeneratedOnAdd();

            builder.Property(c => c.MontoOriginal)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.Monto)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.TipoCambioValor)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.MotivoAnulacion)
                .HasMaxLength(250)
                .IsRequired(false);

            builder.Property(c => c.FechaAnulacion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaOcurrencia)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.MonedaOriginalId)
                .IsRequired(true);

            builder.Property(c => c.MonedaCuentaId)
                .IsRequired(true);

            builder.Property(c => c.TransaccionTipoId)
                .IsRequired(true);

            builder.Property(c => c.CuentaId)
                .IsRequired(true);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.HasOne(c => c.Moneda)
                .WithMany(m => m.TransaccionOriginales)
                .HasForeignKey(c => c.MonedaOriginalId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.MonedaCuenta)
                .WithMany(m => m.Transacciones)
                .HasForeignKey(c => c.MonedaCuentaId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.TransaccionTipo)
                .WithMany(m => m.Transacciones)
                .HasForeignKey(c => c.TransaccionTipoId);
            
            builder.HasOne(c => c.Cuenta)
                .WithMany(m => m.Transacciones)
                .HasForeignKey(c => c.CuentaId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
