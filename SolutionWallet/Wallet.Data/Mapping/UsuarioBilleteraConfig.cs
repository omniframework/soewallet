﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class UsuarioBilleteraConfig : IEntityTypeConfiguration<UsuarioBilletera>
    {
        public void Configure(EntityTypeBuilder<UsuarioBilletera> builder)
        {
            builder
                .HasKey(c => c.UsuarioId);

            builder
                .Property(c => c.UsuarioId)
                .ValueGeneratedNever();

            builder
                .Property(c => c.MonedaId);

            builder.Property(c => c.Balance)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder
                .HasOne(c => c.Moneda)
                .WithMany(x => x.UsuarioBilleteras)
                .HasForeignKey(c => c.MonedaId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
