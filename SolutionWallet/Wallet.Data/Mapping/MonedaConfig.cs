﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class MonedaConfig : IEntityTypeConfiguration<Moneda>
    {
        public void Configure(EntityTypeBuilder<Moneda> builder)
        {
            builder
                .HasKey(c => c.MonedaId);

            builder
                .Property(c => c.MonedaId)
                .ValueGeneratedNever();

            builder.Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);

            builder.Property(c => c.Abreviacion)
                .HasMaxLength(50)
                .IsRequired(true);

            builder.Property(c => c.Descripcion)
                .HasMaxLength(250)
                .IsRequired(false);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));
        }
    }
}
