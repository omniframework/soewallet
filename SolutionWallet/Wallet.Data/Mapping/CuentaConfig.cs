﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class CuentaConfig : IEntityTypeConfiguration<Cuenta>
    {
        public void Configure(EntityTypeBuilder<Cuenta> builder)
        {
            builder
                .HasKey(c => c.CuentaId);            

            builder
                .Property(c => c.CuentaId)
                .ValueGeneratedNever();

            builder.Property(c => c.Balance)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.UsuarioId)
                .IsRequired(true);

            builder.Property(c => c.CuentaTipoId)
                .IsRequired(true);

            builder.Property(c => c.MonedaId)
                .IsRequired(true);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.HasOne(c => c.Moneda)
                .WithMany(m => m.Cuentas)
                .HasForeignKey(c => c.MonedaId);

            builder.HasOne(c => c.CuentaTipo)
                .WithMany(m => m.Cuentas)
                .HasForeignKey(c => c.CuentaTipoId);

            builder.HasOne(c => c.UsuarioBilletera)
                .WithMany(m => m.Cuentas)
                .HasForeignKey(c => c.UsuarioId);
        }
    }
}
