﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;

namespace Wallet.Data.Mapping
{
    public class TipoCambioConfig : IEntityTypeConfiguration<TipoCambio>
    {
        public void Configure(EntityTypeBuilder<TipoCambio> builder)
        {
            builder
                .HasKey(c => new { 
                    c.MonedaOrigenId,
                    c.MonedaDestinoId
                });

            builder.Property(c => c.Equivalencia)
                .HasColumnType(string.Format($"decimal({14},{4})"))
                .IsRequired(true);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));

            builder.HasOne(c => c.MonedaOrigen)
                .WithMany(m => m.TiposCambioOrigen)
                .HasForeignKey(c => c.MonedaOrigenId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(c => c.MonedaDestino)
                .WithMany(m => m.TiposCambioDestino)
                .HasForeignKey(c => c.MonedaDestinoId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
