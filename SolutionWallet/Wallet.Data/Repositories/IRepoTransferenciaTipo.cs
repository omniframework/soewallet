﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;

namespace Wallet.Data.Repositories
{
    public interface IRepoTransferenciaTipo : IRepoBase
    {
        IList<TransferenciaTipoDto> Get();
        bool Exists(short id);
    }
}
