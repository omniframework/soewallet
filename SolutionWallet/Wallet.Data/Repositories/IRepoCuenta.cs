﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Data.Repositories
{
    public interface IRepoCuenta : IRepoBase
    {
        IList<CuentaDto> GetMyAccounts(GetMyAccountsParam param);
        CuentaDto GetBy(long id);
        bool Exists(long id);
    }
}
