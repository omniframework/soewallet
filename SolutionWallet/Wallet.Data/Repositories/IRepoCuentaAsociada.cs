﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Data.Repositories
{
    public interface IRepoCuentaAsociada : IRepoBase
    {
        IList<CuentaAsociadaDto> GetFavoriteAccount(GetFavoriteAccountsParam param);
    }
}
