﻿using Generic.Data.Repositories.Impl;
using Generic.DataTransfer.Enums;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Wallet.Data.Entities;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoMoneda : RepoBase<IWalletUnitOfWork>, IRepoMoneda
    {
        public RepoMoneda(IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public bool Exists(short id)
        {
            return (from m in this.UnitOfWork.Moneda
                    where m.MonedaId == id
                    select m).Any();
        }

        public IList<MonedaDto> Get()
        {
            return (from a in this.UnitOfWork.Moneda
                    where a.Estado != StateEnum.Deleted
                    select new MonedaDto()
                    {
                        MonedaId = a.MonedaId,
                        Abreviacion = a.Abreviacion,
                        Descripcion = a.Descripcion,
                        Name = a.Name,
                        Estado = a.Estado,
                    }).ToList();
        }

        public MonedaDto Get(short id)
        {
            return (from a in this.UnitOfWork.Moneda
                    where a.MonedaId == id
                    select new MonedaDto()
                    {
                        MonedaId = a.MonedaId,
                        Abreviacion = a.Abreviacion,
                        Descripcion = a.Descripcion,
                        Name = a.Name,
                        Estado = a.Estado,                         
                    }).FirstOrDefault();
        }

        public decimal? GetTipoCambio(short idMonedaOrigen, short idMonedaDestino)
        {
            return (from m in this.UnitOfWork.TipoCambio
                    where m.MonedaOrigenId == idMonedaOrigen
                    && m.MonedaDestinoId == idMonedaDestino
                    && m.Estado != StateEnum.Deleted
                    select m.Equivalencia).FirstOrDefault();
        }
    }
}
