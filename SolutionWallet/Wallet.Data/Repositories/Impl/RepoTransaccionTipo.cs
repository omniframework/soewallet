﻿using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Generic.DataTransfer.Enums;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoTransaccionTipo : RepoBase<IWalletUnitOfWork>, IRepoTransaccionTipo
    {
        public RepoTransaccionTipo(IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public bool Exists(short id)
        {
            return (from a in this.UnitOfWork.TransaccionTipo
                    where a.TransaccionTipoId == id
                    && a.Estado != StateEnum.Deleted
                    select a.TransaccionTipoId).Any();
        }

        public TransaccionTipoDto GetBy(short id)
        {
            return (from a in this.UnitOfWork.TransaccionTipo
                    where a.TransaccionTipoId == id
                    && a.Estado != StateEnum.Deleted
                    select new TransaccionTipoDto()
                    {
                        TransaccionTipoId = a.TransaccionTipoId,
                        Descripcion = a.Descripcion,
                        Estado = a.Estado,
                        Nombre = a.Nombre,
                        Ingreso = a.Ingreso,
                        Editable = a.Editable,
                        Seleccionable = a.Seleccionable
                    }).FirstOrDefault();
        }
    }
}
