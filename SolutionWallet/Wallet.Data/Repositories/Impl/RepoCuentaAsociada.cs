﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;
using Generic.Data.Repositories.Impl;
using Generic.DataTransfer.Enums;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoCuentaAsociada : RepoBase<IWalletUnitOfWork>, IRepoCuentaAsociada
    {
        public RepoCuentaAsociada(IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public IList<CuentaAsociadaDto> GetFavoriteAccount(GetFavoriteAccountsParam param)
        {
            return (from f in this.UnitOfWork.CuentaAsociada
                    join a in this.UnitOfWork.Cuenta
                    on f.CuentaId equals a.CuentaId
                    join tc in this.UnitOfWork.CuentaTipo
                    on a.CuentaTipoId equals tc.CuentaTipoId
                    join m in this.UnitOfWork.Moneda
                    on a.MonedaId equals m.MonedaId
                    where a.UsuarioId == param.UsuarioId
                    && a.Estado != StateEnum.Deleted
                    select new CuentaAsociadaDto()
                    {
                        UsuarioId = a.UsuarioId,
                        CuentaId = a.CuentaId,
                        CuentaTipoId = a.CuentaTipoId,
                        CuentaTipoName = tc.Nombre,
                        Alias = f.Alias,
                        CuentaAsociadaId = f.CuentaAsociadaId,
                        MonedaCuentaAbreb = m.Abreviacion,
                        MonedaCuentaId = m.MonedaId,
                        MonedaCuentaName = m.Name                         
                    }).ToList();
        }
    }
}
