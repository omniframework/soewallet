﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;
using Generic.Data.Repositories.Impl;
using Generic.DataTransfer.Enums;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoCuenta : RepoBase<IWalletUnitOfWork>, IRepoCuenta
    {
        public RepoCuenta(IWalletUnitOfWork unitOfWork): base(unitOfWork)
        {
        }

        public bool Exists(long id)
        {
            return (from a in this.UnitOfWork.Cuenta
                    where a.Estado != StateEnum.Deleted
                    select a.CuentaId).Any();
        }

        public CuentaDto GetBy(long id)
        {
            return (from a in this.UnitOfWork.Cuenta
                    join tc in this.UnitOfWork.CuentaTipo
                    on a.CuentaTipoId equals tc.CuentaTipoId
                    join m in this.UnitOfWork.Moneda
                    on a.MonedaId equals m.MonedaId
                    where a.CuentaId == id
                    && a.Estado != StateEnum.Deleted
                    select new CuentaDto()
                    {
                        Balance = a.Balance,
                        UsuarioId = a.UsuarioId,
                        CuentaId = a.CuentaId,
                        MonedaId = a.MonedaId,
                        MonedaAbreb = m.Abreviacion,
                        MonedaName = m.Name,
                        CuentaTipoId = a.CuentaTipoId,
                        CuentaTipoName = tc.Nombre,
                        Estado = tc.Estado,
                    }).FirstOrDefault();
        }

        public IList<CuentaDto> GetMyAccounts(GetMyAccountsParam param)
        {
            return (from a in this.UnitOfWork.Cuenta
                    join tc in this.UnitOfWork.CuentaTipo
                    on a.CuentaTipoId equals tc.CuentaTipoId
                    join m in this.UnitOfWork.Moneda
                    on a.MonedaId equals m.MonedaId
                    where a.UsuarioId == param.UsuarioId
                    && a.Estado != StateEnum.Deleted
                    select new CuentaDto()
                    {
                        Balance = a.Balance,
                        UsuarioId = a.UsuarioId,
                        CuentaId = a.CuentaId,
                        MonedaId = a.MonedaId,
                        MonedaAbreb = m.Abreviacion,
                        MonedaName = m.Name,
                        CuentaTipoId = a.CuentaTipoId,
                        CuentaTipoName = tc.Nombre,
                        Estado = tc.Estado,                         
                    }).ToList();
        }
    }
}
