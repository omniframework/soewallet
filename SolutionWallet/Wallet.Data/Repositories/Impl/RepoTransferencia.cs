﻿using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Generic.DataTransfer.Enums;
using Wallet.Data.Entities;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoTransferencia : RepoBase<IWalletUnitOfWork>, IRepoTransferencia
    {
        public RepoTransferencia(IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public TransferenciaDto Get(long id)
        {
            return (from t in this.UnitOfWork.Transferencia
                    join mo in this.UnitOfWork.Moneda
                    on t.MonedaOrigenId equals mo.MonedaId
                    join mc in this.UnitOfWork.Moneda
                    on t.MonedaDestinoId equals mc.MonedaId
                    join tt in this.UnitOfWork.TransferenciaTipo
                    on t.TransferenciaTipoId equals tt.TransferenciaTipoId
                    where t.TransferenciaId == id
                    && t.Estado != StateEnum.Deleted
                    select new TransferenciaDto()
                    {
                        Monto = t.Monto,
                        TransferenciaId = t.TransferenciaId,
                        Concepto = t.Concepto,
                        CuentaDestinoId = t.CuentaDestinoId,
                        CuentaOrigenId = t.CuentaOrigenId,
                        Descripcion = t.Descripcion,
                        Estado = t.Estado,
                        FechaAnulacion = t.FechaAnulacion,
                        FechaOcurrencia = t.FechaOcurrencia,
                        MontoOriginal = t.MontoOriginal,
                        TipoCambioValor = t.TipoCambioValor,
                        MotivoAnulacion = t.MotivoAnulacion,
                        TransferenciaTipoId = t.TransferenciaTipoId,
                        TransaccionOrigenId = t.TransaccionOrigenId,
                        MonedaDestinoId = t.MonedaDestinoId,
                        MonedaOrigenId = t.MonedaOrigenId,
                        TransaccionDestinoId = t.TransaccionDestinoId,
                        TransferenciaTipo = tt.Nombre,
                        MonedaDestinoAbreb = mc.Abreviacion,
                        MonedaDestinoName = mc.Name,
                        MonedaOrigenAbreb = mo.Abreviacion,
                        MonedaOrigenName = mo.Name
                    }).FirstOrDefault();
        }

        public void PostStore(Transferencia transferencia)
        {
            this.UnitOfWork.Transferencia.Add(transferencia);
        }
    }
}
