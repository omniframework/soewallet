﻿using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Generic.DataTransfer.Enums;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoTransferenciaTipo : RepoBase<IWalletUnitOfWork>, IRepoTransferenciaTipo
    {
        public RepoTransferenciaTipo(IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public bool Exists(short id)
        {
            return (from a in this.UnitOfWork.TransferenciaTipo
                    where a.Estado != StateEnum.Deleted
                    select a.TransferenciaTipoId).Any();
        }

        public IList<TransferenciaTipoDto> Get()
        {
            return (from a in this.UnitOfWork.TransferenciaTipo
                    where a.Estado != StateEnum.Deleted
                    select new TransferenciaTipoDto()
                    {
                        TransferenciaTipoId = a.TransferenciaTipoId,
                        Nombre = a.Nombre,                        
                        Descripcion = a.Descripcion,
                        Estado = a.Estado,
                    }).ToList();
        }
    }
}
