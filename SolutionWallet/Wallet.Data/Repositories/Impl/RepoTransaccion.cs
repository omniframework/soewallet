﻿using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.UnitOfWork;
using Wallet.Entities.DataTransfers;
using Generic.DataTransfer.Enums;
using Wallet.Data.Entities;

namespace Wallet.Data.Repositories.Impl
{
    public class RepoTransaccion : RepoBase<IWalletUnitOfWork>, IRepoTransaccion
    {
        public RepoTransaccion(
            IWalletUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public TransaccionDto Get(long id)
        {
            return (from t in this.UnitOfWork.Transaccion
                    join mo in this.UnitOfWork.Moneda
                    on t.MonedaOriginalId equals mo.MonedaId
                    join mc in this.UnitOfWork.Moneda
                    on t.MonedaCuentaId equals mc.MonedaId
                    join tt in this.UnitOfWork.TransaccionTipo
                    on t.TransaccionTipoId equals tt.TransaccionTipoId
                    where t.TransaccionId == id
                    && t.Estado != StateEnum.Deleted
                    select new TransaccionDto()
                    {
                        Monto = t.Monto,
                        TransaccionId = t.TransaccionId,
                        CuentaId = t.CuentaId,
                        Concepto = t.Concepto,
                        Descripcion = t.Descripcion,
                        Estado = t.Estado,
                        FechaAnulacion = t.FechaAnulacion,
                        FechaOcurrencia = t.FechaOcurrencia,
                        MonedaCuentaId = t.MonedaCuentaId,
                        MonedaOriginalId = t.MonedaOriginalId,
                        TransaccionTipoId = t.TransaccionTipoId,
                        MotivoAnulacion = t.MotivoAnulacion,
                        MontoOriginal = t.MontoOriginal,
                        TipoCambioValor = t.TipoCambioValor,
                        MonedaCuentaAbreb = mc.Abreviacion,
                        MonedaCuentaName = mc.Name,
                        MonedaOrigenAbreb = mo.Abreviacion,
                        MonedaOrigenName = mo.Name,
                        TransaccionTipo = tt.Nombre
                    }).FirstOrDefault();
        }

        public void PostStore(Transaccion transaccion)
        {
            this.UnitOfWork.Transaccion.Add(transaccion);
        }

        public void UpdateAccountBalance(long cuentaId)
        {
            decimal balance = 0;

            var query = (from c in this.UnitOfWork.Transaccion
                         where c.CuentaId == cuentaId
                         && c.Estado == StateEnum.Enabled
                         select c);

            if (query.Count() > 0)
            {
                balance = query.Sum(x => x.Monto);
            }

            Cuenta cuenta = (from c in this.UnitOfWork.Cuenta
                             where c.CuentaId == cuentaId
                             select c).FirstOrDefault();

            cuenta.Balance = balance;
        }

        public void UpdateBilleteraBalance(long usuarioId)
        {
            decimal balance = 0;

            UsuarioBilletera billetera = (from c in this.UnitOfWork.UsuarioBilletera
                                          where c.UsuarioId == usuarioId
                                          select c).FirstOrDefault();

            var cuentas = (from c in this.UnitOfWork.Cuenta
                           join tc in this.UnitOfWork.TipoCambio
                           on new
                           {
                               MonedaOrigenId = c.MonedaId,
                               MonedaDestinoId = billetera.MonedaId,
                           } equals new
                           {
                               tc.MonedaOrigenId,
                               tc.MonedaDestinoId
                           }
                           where c.UsuarioId == usuarioId
                           && c.Estado != StateEnum.Deleted
                           select new
                           {
                               Cuenta = c,
                               tc.Equivalencia
                           }).ToArray();

            long[] ids = cuentas.Select(x => x.Cuenta.CuentaId).ToArray();

            foreach (var cuenta in cuentas)
            {
                balance += cuenta.Cuenta.Balance * cuenta.Equivalencia;
            }

            billetera.Balance = balance;
        }
    }
}
