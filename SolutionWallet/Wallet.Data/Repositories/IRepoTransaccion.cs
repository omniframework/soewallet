﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;
using Wallet.Entities.DataTransfers;

namespace Wallet.Data.Repositories
{
    public interface IRepoTransaccion : IRepoBase
    {
        void PostStore(Transaccion transaccion);
        void UpdateAccountBalance(long cuentaId);
        void UpdateBilleteraBalance(long usuarioId);
        TransaccionDto Get(long id);
    }
}
