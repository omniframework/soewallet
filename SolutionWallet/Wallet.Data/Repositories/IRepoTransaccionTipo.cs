﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;

namespace Wallet.Data.Repositories
{
    public interface IRepoTransaccionTipo : IRepoBase
    {
        bool Exists(short id);
        TransaccionTipoDto GetBy(short id);
    }
}
