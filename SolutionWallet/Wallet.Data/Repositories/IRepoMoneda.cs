﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;
using Wallet.Entities.DataTransfers;

namespace Wallet.Data.Repositories
{
    public interface IRepoMoneda : IRepoBase
    {
        IList<MonedaDto> Get();
        decimal? GetTipoCambio(short idMonedaOrigen, short idMonedaDestino);
        MonedaDto Get(short id);
        bool Exists(short id);
    }
}
