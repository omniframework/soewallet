﻿using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;
using Wallet.Entities.DataTransfers;

namespace Wallet.Data.Repositories
{
    public interface IRepoTransferencia : IRepoBase
    {
        void PostStore(Transferencia transferencia);
        TransferenciaDto Get(long id);
    }
}
