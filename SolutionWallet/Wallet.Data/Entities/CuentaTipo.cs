﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class CuentaTipo
    {
        public short CuentaTipoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }

        public virtual ICollection<Cuenta> Cuentas { get; set; }
        
        public CuentaTipo()
        {
            this.Cuentas = new HashSet<Cuenta>();
        }
    }
}
