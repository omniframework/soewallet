﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class UsuarioBilletera
    {
        public long UsuarioId { get; set; }
        public decimal Balance { get; set; }
        public short MonedaId { get; set; }
        public Moneda Moneda { get; set; }
        public virtual ICollection<Cuenta> Cuentas { get; set; }
        public virtual ICollection<CuentaAsociada> CuentaAsociadas { get; set; }

        public UsuarioBilletera()
        {
            this.Cuentas = new HashSet<Cuenta>();
            this.CuentaAsociadas = new HashSet<CuentaAsociada>();
        }
    }
}
