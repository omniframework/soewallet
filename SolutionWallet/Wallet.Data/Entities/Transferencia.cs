﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class Transferencia
    {
        public long TransferenciaId { get; set; }
        public long TransaccionOrigenId { get; set; }
        public long TransaccionDestinoId { get; set; }
        public long CuentaOrigenId { get; set; }
        public long CuentaDestinoId { get; set; }
        public DateTime FechaOcurrencia { get; set; }
        public decimal MontoOriginal { get; set; }
        public short MonedaOrigenId { get; set; }
        public short MonedaDestinoId { get; set; }
        public decimal TipoCambioValor { get; set; }
        public decimal Monto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }

        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }

        public DateTime? FechaAnulacion { get; set; }
        public string MotivoAnulacion { get; set; }
        public short TransferenciaTipoId { get; set; }

        public virtual Transaccion TransaccionOrigen { get; set; }
        public virtual Transaccion TransaccionDestino { get; set; }
        public virtual Cuenta CuentaOrigen { get; set; }
        public virtual Cuenta CuentaDestino { get; set; }
        public virtual Moneda MonedaOrigen { get; set; }
        public virtual Moneda MonedaDestino { get; set; }

        public virtual TransferenciaTipo TransferenciaTipo { get; set; }
    }
}
