﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class Transaccion
    {
        public long TransaccionId { get; set; }
        public DateTime FechaOcurrencia { get; set; }
        public short TransaccionTipoId { get; set; }
        public long CuentaId { get; set; }
        public short MonedaOriginalId { get; set; }
        public short MonedaCuentaId { get; set; }
        public decimal TipoCambioValor { get; set; }
        public decimal MontoOriginal { get; set; }
        public decimal Monto { get; set; }
        public string Concepto { get; set; }
        public string Descripcion { get; set; }
        public string MotivoAnulacion { get; set; }
        public DateTime? FechaAnulacion { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }

        public virtual Moneda Moneda { get; set; }
        public virtual Moneda MonedaCuenta { get; set; }
        public virtual Cuenta Cuenta { get; set; }
        public virtual TransaccionTipo TransaccionTipo { get; set; }
        public virtual ICollection<Transferencia> TransferenciasOrigen { get; set; }
        public virtual ICollection<Transferencia> TransferenciasDestino { get; set; }

        public Transaccion()
        {
            this.TransferenciasOrigen = new HashSet<Transferencia>();
            this.TransferenciasDestino = new HashSet<Transferencia>();
        }
    }
}
