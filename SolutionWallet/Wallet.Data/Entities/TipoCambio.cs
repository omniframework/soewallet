﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class TipoCambio
    {
        public short MonedaOrigenId { get; set; }
        public short MonedaDestinoId { get; set; }
        public decimal Equivalencia { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }

        public virtual Moneda MonedaOrigen { get; set; }
        public virtual Moneda MonedaDestino { get; set; }
    }
}
