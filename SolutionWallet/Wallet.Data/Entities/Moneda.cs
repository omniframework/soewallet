﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class Moneda
    {
        public short MonedaId { get; set; }
        public string Abreviacion { get; set; }
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }
        public virtual ICollection<UsuarioBilletera> UsuarioBilleteras { get; set; }
        public virtual ICollection<Transaccion> TransaccionOriginales { get; set; }
        public virtual ICollection<Transaccion> Transacciones { get; set; }
        public virtual ICollection<Cuenta> Cuentas { get; set; }
        public virtual ICollection<TipoCambio> TiposCambioOrigen { get; set; }
        public virtual ICollection<TipoCambio> TiposCambioDestino { get; set; }
        public virtual ICollection<Transferencia> TransferenciaEnviadas { get; set; }
        public virtual ICollection<Transferencia> TransferenciaRecividas { get; set; }

        public Moneda()
        {
            this.TransaccionOriginales = new HashSet<Transaccion>();
            this.Transacciones = new HashSet<Transaccion>();
            this.Cuentas = new HashSet<Cuenta>();
            this.TiposCambioOrigen = new HashSet<TipoCambio>();
            this.TiposCambioDestino = new HashSet<TipoCambio>();
            this.TransferenciaEnviadas = new HashSet<Transferencia>();
            this.TransferenciaRecividas = new HashSet<Transferencia>();
            this.UsuarioBilleteras = new HashSet<UsuarioBilletera>();
        }
    }
}
