﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class TransaccionTipo
    {
        public short TransaccionTipoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Ingreso { get; set; }
        public bool Editable { get; set; }
        public bool Seleccionable { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }
        public virtual ICollection<Transaccion> Transacciones { get; set; }

        public TransaccionTipo()
        {
            this.Transacciones = new HashSet<Transaccion>();
        }
    }
}
