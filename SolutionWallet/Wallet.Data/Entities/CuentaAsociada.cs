﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class CuentaAsociada
    {
        public long CuentaAsociadaId { get; set; }
        public long CuentaId { get; set; }
        public long UsuarioId { get; set; }        
        public string Alias { get; set; }

        public virtual UsuarioBilletera UsuarioBilletera { get; set; }
        public virtual Cuenta Cuenta { get; set; }
    }
}
