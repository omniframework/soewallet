﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class Cuenta
    {
        public long CuentaId { get; set; }
        public long UsuarioId { get; set; }
        public decimal Balance { get; set; }
        public short CuentaTipoId { get; set; }
        public short MonedaId { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }
        public virtual CuentaTipo CuentaTipo { get; set; }
        public virtual Moneda Moneda { get; set; }
        public virtual UsuarioBilletera UsuarioBilletera { get; set; }
        public virtual ICollection<CuentaAsociada> CuentaAsociadas { get; set; }
        public virtual ICollection<Transaccion> Transacciones { get; set; }
        public virtual ICollection<Transferencia> TransferenciaEnviadas { get; set; }
        public virtual ICollection<Transferencia> TransferenciaRecividas { get; set; }

        public Cuenta()
        {
            this.CuentaAsociadas = new HashSet<CuentaAsociada>();
            this.Transacciones = new HashSet<Transaccion>();
            this.TransferenciaEnviadas = new HashSet<Transferencia>();
            this.TransferenciaRecividas = new HashSet<Transferencia>();
        }
    }
}
