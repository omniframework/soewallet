﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallet.Data.Entities
{
    public class TransferenciaTipo
    {
        public short TransferenciaTipoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }
        public virtual ICollection<Transferencia> Transferencias { get; set; }

        public TransferenciaTipo()
        {
            this.Transferencias = new HashSet<Transferencia>();
        }
    }
}
