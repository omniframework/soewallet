﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Data.UnitOfWork.Impl
{
    public class DbContextUnitOfWork : DbContext, IUnitOfWorkBase
    {
        public DbContextUnitOfWork(DbContextOptions options) : base(options)
        {
        }
    }
}
