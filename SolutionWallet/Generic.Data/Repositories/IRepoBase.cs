﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Data.Repositories
{
    public interface IRepoBase : IDisposable
    {
        int SaveChanges();
    }
}
