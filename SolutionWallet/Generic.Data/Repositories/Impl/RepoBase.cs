﻿using Generic.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Data.Repositories.Impl
{
    public abstract class RepoBase<TUnitOfWorkBase> : IRepoBase
        where TUnitOfWorkBase : IUnitOfWorkBase
    {
        public TUnitOfWorkBase UnitOfWork { get; set; }

        public RepoBase(
            TUnitOfWorkBase unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public void Dispose()
        {
            this.UnitOfWork?.Dispose();
            this.UnitOfWork = default;
        }

        public int SaveChanges()
        {
            return this.UnitOfWork.SaveChanges();
        }
    }
}
