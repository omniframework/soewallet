﻿using Generic.Core.Services;
using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Api.Controllers
{
    public class AppControllerBase : ControllerBase, IDisposable
    {
        protected ApiResponse<TData> ProcessReponse<TData>(TData data, string message = null)
        {
            return new ApiResponse<TData>()
            {
                Codigo = (short)ApiResponseEnum.Success,
                Data = data,
                Mensaje = message                 
            };
        }

        public virtual void Dispose()
        {
        }
    }

    public class AppControllerBase<TService> : AppControllerBase
        where TService : IServiceBase
    {
        protected TService _mainService;

        public AppControllerBase(TService mainService)
        {
            this._mainService = mainService;
        }

        public override void Dispose()
        {
            this._mainService?.Dispose();
            this._mainService = default;
        }
    }
}
