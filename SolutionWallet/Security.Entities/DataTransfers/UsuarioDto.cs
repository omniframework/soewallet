﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Entities.DataTransfers
{
    public class UsuarioDto
    {
        public long UsuarioId { get; set; }
        public string NickName { get; set; }
        public string NombreCompleto { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public bool PrimerInicio { get; set; }
        public short Estado { get; set; }
    }
}
