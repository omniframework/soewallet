﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Entities.Params
{
    public class ChangePasswordParam
    {
        public long UsuarioId { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}
