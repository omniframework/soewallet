﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Entities.Params
{
    public class LoginParam
    {
        public string NickName { get; set; }
        public string Password { get; set; }
    }
}
