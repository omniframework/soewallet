﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConsoleApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CuentaTipo",
                columns: table => new
                {
                    CuentaTipoId = table.Column<short>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 255, nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CuentaTipo", x => x.CuentaTipoId);
                });

            migrationBuilder.CreateTable(
                name: "Moneda",
                columns: table => new
                {
                    MonedaId = table.Column<short>(nullable: false),
                    Abreviacion = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 250, nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Moneda", x => x.MonedaId);
                });

            migrationBuilder.CreateTable(
                name: "TransaccionTipo",
                columns: table => new
                {
                    TransaccionTipoId = table.Column<short>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 255, nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransaccionTipo", x => x.TransaccionTipoId);
                });

            migrationBuilder.CreateTable(
                name: "TransferenciaTipo",
                columns: table => new
                {
                    TransferenciaTipoId = table.Column<short>(nullable: false),
                    Nombre = table.Column<string>(maxLength: 100, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 255, nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransferenciaTipo", x => x.TransferenciaTipoId);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    UsuarioId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NickName = table.Column<string>(maxLength: 50, nullable: false),
                    Password = table.Column<string>(maxLength: 150, nullable: false),
                    NombreCompleto = table.Column<string>(maxLength: 305, nullable: false),
                    PrimerNombre = table.Column<string>(maxLength: 150, nullable: false),
                    SegundoNombre = table.Column<string>(maxLength: 150, nullable: true),
                    Telefono = table.Column<string>(maxLength: 20, nullable: true),
                    CorreoElectronico = table.Column<string>(maxLength: 100, nullable: false),
                    PrimerInicio = table.Column<bool>(nullable: false),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.UsuarioId);
                });

            migrationBuilder.CreateTable(
                name: "UsuarioBilletera",
                columns: table => new
                {
                    UsuarioId = table.Column<long>(nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(14,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuarioBilletera", x => x.UsuarioId);
                });

            migrationBuilder.CreateTable(
                name: "TipoCambio",
                columns: table => new
                {
                    MonedaOrigenId = table.Column<short>(nullable: false),
                    MonedaDestinoId = table.Column<short>(nullable: false),
                    Equivalencia = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoCambio", x => new { x.MonedaOrigenId, x.MonedaDestinoId });
                    table.ForeignKey(
                        name: "FK_TipoCambio_Moneda_MonedaDestinoId",
                        column: x => x.MonedaDestinoId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                    table.ForeignKey(
                        name: "FK_TipoCambio_Moneda_MonedaOrigenId",
                        column: x => x.MonedaOrigenId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                });

            migrationBuilder.CreateTable(
                name: "Cuenta",
                columns: table => new
                {
                    CuentaId = table.Column<long>(nullable: false),
                    UsuarioId = table.Column<long>(nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    CuentaTipoId = table.Column<short>(nullable: false),
                    MonedaId = table.Column<short>(nullable: false),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cuenta", x => x.CuentaId);
                    table.ForeignKey(
                        name: "FK_Cuenta_CuentaTipo_CuentaTipoId",
                        column: x => x.CuentaTipoId,
                        principalTable: "CuentaTipo",
                        principalColumn: "CuentaTipoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cuenta_Moneda_MonedaId",
                        column: x => x.MonedaId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cuenta_UsuarioBilletera_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "UsuarioBilletera",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CuentaAsociada",
                columns: table => new
                {
                    CuentaAsociadaId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CuentaId = table.Column<long>(nullable: false),
                    UsuarioId = table.Column<long>(nullable: false),
                    Alias = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CuentaAsociada", x => x.CuentaAsociadaId);
                    table.ForeignKey(
                        name: "FK_CuentaAsociada_Cuenta_CuentaId",
                        column: x => x.CuentaId,
                        principalTable: "Cuenta",
                        principalColumn: "CuentaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CuentaAsociada_UsuarioBilletera_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "UsuarioBilletera",
                        principalColumn: "UsuarioId");
                });

            migrationBuilder.CreateTable(
                name: "Transaccion",
                columns: table => new
                {
                    TransaccionId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FechaOcurrencia = table.Column<DateTime>(type: "datetime", nullable: false),
                    TransaccionTipoId = table.Column<short>(nullable: false),
                    CuentaId = table.Column<long>(nullable: false),
                    MonedaOriginalId = table.Column<short>(nullable: false),
                    MonedaCuentaId = table.Column<short>(nullable: false),
                    TipoCambioValor = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    MontoOriginal = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    Monto = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    Concepto = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    MotivoAnulacion = table.Column<string>(maxLength: 250, nullable: true),
                    FechaAnulacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transaccion", x => x.TransaccionId);
                    table.ForeignKey(
                        name: "FK_Transaccion_Cuenta_CuentaId",
                        column: x => x.CuentaId,
                        principalTable: "Cuenta",
                        principalColumn: "CuentaId");
                    table.ForeignKey(
                        name: "FK_Transaccion_Moneda_MonedaCuentaId",
                        column: x => x.MonedaCuentaId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                    table.ForeignKey(
                        name: "FK_Transaccion_Moneda_MonedaOriginalId",
                        column: x => x.MonedaOriginalId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                    table.ForeignKey(
                        name: "FK_Transaccion_TransaccionTipo_TransaccionTipoId",
                        column: x => x.TransaccionTipoId,
                        principalTable: "TransaccionTipo",
                        principalColumn: "TransaccionTipoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transferencia",
                columns: table => new
                {
                    TransferenciaId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransaccionOrigenId = table.Column<long>(nullable: false),
                    TransaccionDestinoId = table.Column<long>(nullable: false),
                    CuentaOrigenId = table.Column<long>(nullable: false),
                    CuentaDestinoId = table.Column<long>(nullable: false),
                    FechaOcurrencia = table.Column<DateTime>(type: "datetime", nullable: false),
                    MontoOriginal = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    MonedaOrigenId = table.Column<short>(nullable: false),
                    MonedaDestinoId = table.Column<short>(nullable: false),
                    TipoCambioValor = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    Monto = table.Column<decimal>(type: "decimal(14,4)", nullable: false),
                    Concepto = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Estado = table.Column<short>(nullable: false),
                    FechaCreacion = table.Column<DateTime>(type: "datetime", nullable: false),
                    FechaEdicion = table.Column<DateTime>(type: "datetime", nullable: true),
                    UsuarioCreacion = table.Column<long>(nullable: true),
                    UsuarioEdicion = table.Column<long>(nullable: true),
                    FechaAnulacion = table.Column<DateTime>(type: "datetime", nullable: true),
                    MotivoAnulacion = table.Column<string>(maxLength: 250, nullable: true),
                    TransferenciaTipoId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transferencia", x => x.TransferenciaId);
                    table.ForeignKey(
                        name: "FK_Transferencia_Cuenta_CuentaDestinoId",
                        column: x => x.CuentaDestinoId,
                        principalTable: "Cuenta",
                        principalColumn: "CuentaId");
                    table.ForeignKey(
                        name: "FK_Transferencia_Cuenta_CuentaOrigenId",
                        column: x => x.CuentaOrigenId,
                        principalTable: "Cuenta",
                        principalColumn: "CuentaId");
                    table.ForeignKey(
                        name: "FK_Transferencia_Moneda_MonedaDestinoId",
                        column: x => x.MonedaDestinoId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                    table.ForeignKey(
                        name: "FK_Transferencia_Moneda_MonedaOrigenId",
                        column: x => x.MonedaOrigenId,
                        principalTable: "Moneda",
                        principalColumn: "MonedaId");
                    table.ForeignKey(
                        name: "FK_Transferencia_Transaccion_TransaccionDestinoId",
                        column: x => x.TransaccionDestinoId,
                        principalTable: "Transaccion",
                        principalColumn: "TransaccionId");
                    table.ForeignKey(
                        name: "FK_Transferencia_Transaccion_TransaccionOrigenId",
                        column: x => x.TransaccionOrigenId,
                        principalTable: "Transaccion",
                        principalColumn: "TransaccionId");
                    table.ForeignKey(
                        name: "FK_Transferencia_TransferenciaTipo_TransferenciaTipoId",
                        column: x => x.TransferenciaTipoId,
                        principalTable: "TransferenciaTipo",
                        principalColumn: "TransferenciaTipoId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cuenta_CuentaTipoId",
                table: "Cuenta",
                column: "CuentaTipoId");

            migrationBuilder.CreateIndex(
                name: "IX_Cuenta_MonedaId",
                table: "Cuenta",
                column: "MonedaId");

            migrationBuilder.CreateIndex(
                name: "IX_Cuenta_UsuarioId",
                table: "Cuenta",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_CuentaAsociada_CuentaId",
                table: "CuentaAsociada",
                column: "CuentaId");

            migrationBuilder.CreateIndex(
                name: "IX_CuentaAsociada_UsuarioId",
                table: "CuentaAsociada",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_TipoCambio_MonedaDestinoId",
                table: "TipoCambio",
                column: "MonedaDestinoId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaccion_CuentaId",
                table: "Transaccion",
                column: "CuentaId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaccion_MonedaCuentaId",
                table: "Transaccion",
                column: "MonedaCuentaId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaccion_MonedaOriginalId",
                table: "Transaccion",
                column: "MonedaOriginalId");

            migrationBuilder.CreateIndex(
                name: "IX_Transaccion_TransaccionTipoId",
                table: "Transaccion",
                column: "TransaccionTipoId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_CuentaDestinoId",
                table: "Transferencia",
                column: "CuentaDestinoId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_CuentaOrigenId",
                table: "Transferencia",
                column: "CuentaOrigenId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_MonedaDestinoId",
                table: "Transferencia",
                column: "MonedaDestinoId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_MonedaOrigenId",
                table: "Transferencia",
                column: "MonedaOrigenId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_TransaccionDestinoId",
                table: "Transferencia",
                column: "TransaccionDestinoId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_TransaccionOrigenId",
                table: "Transferencia",
                column: "TransaccionOrigenId");

            migrationBuilder.CreateIndex(
                name: "IX_Transferencia_TransferenciaTipoId",
                table: "Transferencia",
                column: "TransferenciaTipoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CuentaAsociada");

            migrationBuilder.DropTable(
                name: "TipoCambio");

            migrationBuilder.DropTable(
                name: "Transferencia");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Transaccion");

            migrationBuilder.DropTable(
                name: "TransferenciaTipo");

            migrationBuilder.DropTable(
                name: "Cuenta");

            migrationBuilder.DropTable(
                name: "TransaccionTipo");

            migrationBuilder.DropTable(
                name: "CuentaTipo");

            migrationBuilder.DropTable(
                name: "Moneda");

            migrationBuilder.DropTable(
                name: "UsuarioBilletera");
        }
    }
}
