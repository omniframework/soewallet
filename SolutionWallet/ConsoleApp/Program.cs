﻿using Microsoft.EntityFrameworkCore;
using Security.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Wallet.Data.Entities;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new Context())
            {
                if (!context.Database.EnsureCreated())
                {
                    Console.WriteLine("La base de datos ya existe!");
                    string[] pendingMigrations = context.Database.GetPendingMigrations().ToArray();
                    if (pendingMigrations.Any())
                    {
                        string tmpMigration = "";
                        if (pendingMigrations.Contains(tmpMigration)) // por cada migración con seeder
                        {

                            Console.WriteLine($"Seeder ejecutado para migración \"{tmpMigration}\"!");
                        }
                        context.Database.Migrate();
                        Console.WriteLine("Migraciones ejecutadas exitosamente!");
                    }
                }
                else
                {
                    Console.WriteLine("Se ha creado la base de datos correctamente!");
                    context.Usuario.Add(new Usuario()
                    {
                        UsuarioId = 1,
                        NombreCompleto = "Administrador",
                        PrimerInicio = false,
                        Password = "2e33a9b0b06aa0a01ede70995674ee23", // Admin1
                        PrimerNombre = "Admin",
                        SegundoNombre = null,
                        Telefono = null,
                        CorreoElectronico = "admin@admin.com",
                        NickName = "SAdmin",
                        Estado = 1,
                        FechaCreacion = DateTime.Now,
                        FechaEdicion = DateTime.Now,
                        UsuarioCreacion = 1,
                        UsuarioEdicion = 1,
                    });
                    context.Moneda.AddRange(new Moneda[] {
                        new Moneda()
                        {
                            MonedaId = 1,
                            Abreviacion = "Bs",
                            Name = "Bolivianos",
                            Descripcion = "Bolivianos",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new Moneda()
                        {
                            MonedaId = 2,
                            Abreviacion = "Bs",
                            Name = "Bolivianos",
                            Descripcion = "Bolivianos",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        }
                    });
                    context.TipoCambio.AddRange(new TipoCambio[]{
                        new TipoCambio(){
                            Equivalencia = 1,
                            MonedaDestinoId = 1,
                            MonedaOrigenId = 1,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TipoCambio(){
                            Equivalencia = 1,
                            MonedaDestinoId = 2,
                            MonedaOrigenId = 2,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TipoCambio(){
                            Equivalencia = (1.0m / 6.96m),
                            MonedaOrigenId = 1,
                            MonedaDestinoId = 2,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TipoCambio(){
                            Equivalencia = 6.96m,
                            MonedaOrigenId = 2,
                            MonedaDestinoId = 1,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        }
                    });
                    context.CuentaTipo.AddRange(new CuentaTipo[] {
                        new CuentaTipo(){
                            CuentaTipoId = 1,
                            Nombre = "Billetera Generica",
                            Descripcion = "Billetera Generica",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        }
                    });
                    context.TransaccionTipo.AddRange(new TransaccionTipo[] {
                        new TransaccionTipo(){
                            TransaccionTipoId = 1,
                            Nombre = "Ingreso",
                            Descripcion = "Ingreso",
                            Ingreso = true,
                            Seleccionable = true,
                            Editable = false,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransaccionTipo(){
                            TransaccionTipoId = 2,
                            Nombre = "Egreso",
                            Descripcion = "Egreso",
                            Ingreso = false,
                            Seleccionable = true,
                            Editable = false,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransaccionTipo(){
                            TransaccionTipoId = 3,
                            Nombre = "Transferencia Ingreso",
                            Descripcion = "Transferencia Ingreso",
                            Ingreso = true,
                            Seleccionable = false,
                            Editable = false,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransaccionTipo(){
                            TransaccionTipoId = 4,
                            Nombre = "Transferencia Egreso",
                            Descripcion = "Transferencia Egreso",
                            Ingreso = false,
                            Seleccionable = false,
                            Editable = false,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransaccionTipo(){
                            TransaccionTipoId = 1000,
                            Nombre = "Tarjeta 100 bs",
                            Descripcion = "Tarjeta 100 bs",
                            Ingreso = true,
                            Seleccionable = true,
                            Editable = true,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransaccionTipo(){
                            TransaccionTipoId = 1001,
                            Nombre = "Cobro de servicio",
                            Descripcion = "Cobro de servicio",
                            Ingreso = false,
                            Seleccionable = true,
                            Editable = true,
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        }
                    });
                    context.TransferenciaTipo.AddRange(new TransferenciaTipo[] {
                        new TransferenciaTipo(){
                            TransferenciaTipoId = 1,
                            Nombre = "Cuentras propias",
                            Descripcion = "Cuentras propias",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        },
                        new TransferenciaTipo(){
                            TransferenciaTipoId = 2,
                            Nombre = "Cuentras de terceros",
                            Descripcion = "Cuentras de terceros",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1
                        }
                    });

                    #region Usuarios dummie
                    context.Usuario.AddRange(new Usuario[]
                    {
                        new Usuario()
                        {
                            UsuarioId = 2,
                            NombreCompleto = "José Moscoso",
                            PrimerInicio = false,
                            Password = "2e33a9b0b06aa0a01ede70995674ee23",
                            PrimerNombre = "José",
                            SegundoNombre = "Moscoso",
                            Telefono = "78080555",
                            CorreoElectronico = "daherdelfin@gmail.com",
                            NickName = "moscoso",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1,
                        },
                        new Usuario()
                        {
                            UsuarioId = 3,
                            NombreCompleto = "Victor Hugo Vargas Salvatierra",
                            PrimerInicio = false,
                            Password = "2e33a9b0b06aa0a01ede70995674ee23",
                            PrimerNombre = "Victor Hugo ",
                            SegundoNombre = "Vargas Salvatierra",
                            Telefono = "78080555",
                            CorreoElectronico = "jesusv@gmail.com",
                            NickName = "jesusv",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1,
                        },
                        new Usuario()
                        {
                            UsuarioId = 4,
                            NombreCompleto = "Gonzalo Ernesto Pérez Morón",
                            PrimerInicio = false,
                            Password = "2e33a9b0b06aa0a01ede70995674ee23",
                            PrimerNombre = "Gonzalo Ernesto",
                            SegundoNombre = "Pérez Morón",
                            Telefono = "75346027",
                            CorreoElectronico = "ernesto_06_05@hotmail.com",
                            NickName = "ernesto1",
                            Estado = 1,
                            FechaCreacion = DateTime.Now,
                            FechaEdicion = DateTime.Now,
                            UsuarioCreacion = 1,
                            UsuarioEdicion = 1,
                        }
                    });
                    context.UsuarioBilletera.AddRange(new UsuarioBilletera[]
                    {
                        new UsuarioBilletera()
                        {
                             UsuarioId = 1,
                             Balance = 1696,
                             MonedaId = 1,
                             Cuentas = new HashSet<Cuenta>()
                             {
                                 new Cuenta()
                                 {
                                     UsuarioId = 1,
                                     CuentaId = 1,
                                     CuentaTipoId = 1,
                                     Balance = 1000,
                                     MonedaId = 1,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 1,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 1,
                                             MonedaOriginalId = 1,
                                             MontoOriginal = 1000,
                                             Monto = 1000,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 },
                                 new Cuenta()
                                 {
                                     UsuarioId = 1,
                                     CuentaId = 2,
                                     CuentaTipoId = 1,
                                     Balance = 100,
                                     MonedaId = 2,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 2,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 2,
                                             MonedaOriginalId = 2,
                                             MontoOriginal = 100,
                                             Monto = 100,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 }
                             },
                             CuentaAsociadas = new HashSet<CuentaAsociada>()
                             {
                                 new CuentaAsociada()
                                 {
                                      Alias = "José Moscoso 1",
                                      CuentaId = 3,
                                      UsuarioId = 1                                       
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "José Moscoso 2",
                                      CuentaId = 4,
                                      UsuarioId = 1
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "Hugo Vargas 1",
                                      CuentaId = 5,
                                      UsuarioId = 1
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Hugo Vargas 2",
                                      CuentaId = 6,
                                      UsuarioId = 1
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "Gonzalo Perez 1",
                                      CuentaId = 7,
                                      UsuarioId = 1
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Gonzalo Perez 2",
                                      CuentaId = 8,
                                      UsuarioId = 1
                                 }
                             }
                        },
                        new UsuarioBilletera()
                        {
                             UsuarioId = 2,
                             Balance = 1696,
                             MonedaId = 1,
                             Cuentas = new HashSet<Cuenta>()
                             {
                                 new Cuenta()
                                 {
                                     UsuarioId = 2,
                                     CuentaId = 3,
                                     CuentaTipoId = 1,
                                     Balance = 1000,
                                     MonedaId = 1,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 3,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 1,
                                             MonedaOriginalId = 1,
                                             MontoOriginal = 1000,
                                             Monto = 1000,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 },
                                 new Cuenta()
                                 {
                                     UsuarioId = 2,
                                     CuentaId = 4,
                                     CuentaTipoId = 1,
                                     Balance = 100,
                                     MonedaId = 2,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 4,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 2,
                                             MonedaOriginalId = 2,
                                             MontoOriginal = 100,
                                             Monto = 100,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 }
                             },
                             CuentaAsociadas = new HashSet<CuentaAsociada>()
                             {
                                 new CuentaAsociada()
                                 {
                                      Alias = "Administrador 1",
                                      CuentaId = 1,
                                      UsuarioId = 2
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Administrador 2",
                                      CuentaId = 2,
                                      UsuarioId = 2
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "Hugo Vargas 1",
                                      CuentaId = 5,
                                      UsuarioId = 2
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Hugo Vargas 2",
                                      CuentaId = 6,
                                      UsuarioId = 2
                                 },

                                 new CuentaAsociada()
                                 {
                                      Alias = "Gonzalo Perez 1",
                                      CuentaId = 7,
                                      UsuarioId = 2
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Gonzalo Perez 2",
                                      CuentaId = 8,
                                      UsuarioId = 2
                                 }
                             }
                        },
                        new UsuarioBilletera()
                        {
                             UsuarioId = 3,
                             Balance = 1696,
                             MonedaId = 1,
                             Cuentas = new HashSet<Cuenta>()
                             {
                                 new Cuenta()
                                 {
                                     UsuarioId = 3,
                                     CuentaId = 5,
                                     CuentaTipoId = 1,
                                     Balance = 1000,
                                     MonedaId = 1,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 5,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 1,
                                             MonedaOriginalId = 1,
                                             MontoOriginal = 1000,
                                             Monto = 1000,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 },
                                 new Cuenta()
                                 {
                                     UsuarioId = 3,
                                     CuentaId = 6,
                                     CuentaTipoId = 1,
                                     Balance = 100,
                                     MonedaId = 2,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 6,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 2,
                                             MonedaOriginalId = 2,
                                             MontoOriginal = 100,
                                             Monto = 100,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 }
                             },
                             CuentaAsociadas = new HashSet<CuentaAsociada>()
                             {
                                 new CuentaAsociada()
                                 {
                                      Alias = "Administrador 1",
                                      CuentaId = 1,
                                      UsuarioId = 3
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Administrador 2",
                                      CuentaId = 2,
                                      UsuarioId = 3
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "José Moscoso 1",
                                      CuentaId = 3,
                                      UsuarioId = 3
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "José Moscoso 2",
                                      CuentaId = 4,
                                      UsuarioId = 3
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "Gonzalo Perez 1",
                                      CuentaId = 7,
                                      UsuarioId = 3
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Gonzalo Perez 2",
                                      CuentaId = 8,
                                      UsuarioId = 3
                                 }
                             }
                        },
                        new UsuarioBilletera()
                        {
                             UsuarioId = 4,
                             Balance = 1696,
                             MonedaId = 1,
                             Cuentas = new HashSet<Cuenta>()
                             {
                                 new Cuenta()
                                 {
                                     UsuarioId = 4,
                                     CuentaId = 7,
                                     CuentaTipoId = 1,
                                     Balance = 1000,
                                     MonedaId = 1,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 7,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 1,
                                             MonedaOriginalId = 1,
                                             MontoOriginal = 1000,
                                             Monto = 1000,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 },
                                 new Cuenta()
                                 {
                                     UsuarioId = 4,
                                     CuentaId = 8,
                                     CuentaTipoId = 1,
                                     Balance = 100,
                                     MonedaId = 2,
                                     Estado = 1,
                                     FechaCreacion = DateTime.Now,
                                     FechaEdicion = DateTime.Now,
                                     UsuarioCreacion = 1,
                                     UsuarioEdicion = 1,
                                     Transacciones = new HashSet<Transaccion>()
                                     {
                                         new Transaccion()
                                         {
                                             CuentaId = 8,
                                             Concepto = "Carga Inicial",
                                             Descripcion = "Carga Inicial",
                                             FechaOcurrencia = DateTime.Now,
                                             MonedaCuentaId = 2,
                                             MonedaOriginalId = 2,
                                             MontoOriginal = 100,
                                             Monto = 100,
                                             TipoCambioValor = 1,
                                             TransaccionTipoId = 1,
                                             Estado = 1,
                                             FechaCreacion = DateTime.Now,
                                             FechaEdicion = DateTime.Now,
                                             UsuarioCreacion = 1,
                                             UsuarioEdicion = 1,
                                         }
                                     }
                                 }
                             },
                             CuentaAsociadas = new HashSet<CuentaAsociada>()
                             {
                                 new CuentaAsociada()
                                 {
                                      Alias = "Administrador 1",
                                      CuentaId = 1,
                                      UsuarioId = 4
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Administrador 2",
                                      CuentaId = 2,
                                      UsuarioId = 4
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "José Moscoso 1",
                                      CuentaId = 3,
                                      UsuarioId = 4
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "José Moscoso 2",
                                      CuentaId = 4,
                                      UsuarioId = 4
                                 },
                                 new CuentaAsociada()
                                 {
                                      Alias = "Hugo Vargas 1",
                                      CuentaId = 5,
                                      UsuarioId = 4
                                 },
                                 new CuentaAsociada()
                                 {
                                     Alias = "Hugo Vargas 2",
                                      CuentaId = 6,
                                      UsuarioId = 4
                                 }
                             }
                        }
                    });
                    #endregion

                    context.SaveChanges();

                    Console.WriteLine("Ejecución de seeder exitosa!");
                }
            }

            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }
    }
}
