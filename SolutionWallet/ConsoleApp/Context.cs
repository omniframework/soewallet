﻿using Microsoft.EntityFrameworkCore;
using Security.Data.Entities;
using Security.Data.Mapping;
using Security.Data.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Entities;
using Wallet.Data.Mapping;
using Wallet.Data.UnitOfWork;

namespace ConsoleApp
{
    public class Context : DbContext, ISecurityUnitOfWork, IWalletUnitOfWork
    {
        public Context() : base()
        {
        }

        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Cuenta> Cuenta { get; set; }
        public virtual DbSet<CuentaAsociada> CuentaAsociada { get; set; }
        public virtual DbSet<CuentaTipo> CuentaTipo { get; set; }
        public virtual DbSet<Moneda> Moneda { get; set; }
        public virtual DbSet<TipoCambio> TipoCambio { get; set; }
        public virtual DbSet<Transaccion> Transaccion { get; set; }
        public virtual DbSet<TransaccionTipo> TransaccionTipo { get; set; }
        public virtual DbSet<Transferencia> Transferencia { get; set; }
        public virtual DbSet<TransferenciaTipo> TransferenciaTipo { get; set; }
        public virtual DbSet<UsuarioBilletera> UsuarioBilletera { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(
                //@"Server=SC01SISNODO\DORTIZV;User ID=desa;Password=S3gUr01820;Database=bd_wallet;Integrated Security=True;");
                @"Server=.;User ID=user;Password=user;Database=bd_wallet;Integrated Security=True;");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UsuarioConfig());
            modelBuilder.ApplyConfiguration(new CuentaAsociadaConfig());
            modelBuilder.ApplyConfiguration(new CuentaConfig());
            modelBuilder.ApplyConfiguration(new CuentaTipoConfig());
            modelBuilder.ApplyConfiguration(new MonedaConfig());
            modelBuilder.ApplyConfiguration(new TipoCambioConfig());
            modelBuilder.ApplyConfiguration(new TransaccionConfig());
            modelBuilder.ApplyConfiguration(new TransferenciaTipoConfig());
            modelBuilder.ApplyConfiguration(new TransferenciaConfig());
            modelBuilder.ApplyConfiguration(new TransaccionTipoConfig());
            modelBuilder.ApplyConfiguration(new UsuarioBilleteraConfig());
        }
    }
}
