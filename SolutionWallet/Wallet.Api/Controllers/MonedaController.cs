﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core;
using Wallet.Entities.DataTransfers;

namespace Wallet.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MonedaController : AppControllerBase<IServiceMoneda>
    {
        public MonedaController(IServiceMoneda mainService) : base(mainService)
        {
        }

        [HttpGet]
        public ApiResponse<IList<MonedaDto>> Get()
        {
            return ProcessReponse(this._mainService.Get());
        }
    }
}
