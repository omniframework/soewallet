﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Api.Controllers
{
    [ApiController]
    [Route("cuenta_asociada")]
    public class CuentaAsociadaController : AppControllerBase<IServiceCuentaAsociada>
    {
        public CuentaAsociadaController(IServiceCuentaAsociada mainService) : base(mainService)
        {
        }

        [HttpPost]
        [Route("favorite_account")]
        public ApiResponse<IList<CuentaAsociadaDto>> GetFavoriteAccount([FromBody] GetFavoriteAccountsParam param)
        {
            return ProcessReponse(this._mainService.GetFavoriteAccount(param));
        }
    }
}
