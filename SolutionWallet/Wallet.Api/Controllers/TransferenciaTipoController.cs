﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core;
using Wallet.Entities.DataTransfers;

namespace Wallet.Api.Controllers
{
    [ApiController]
    [Route("transferencia_tipo")]
    public class TransferenciaTipoController : AppControllerBase<IServiceTransferenciaTipo>
    {
        public TransferenciaTipoController(IServiceTransferenciaTipo mainService) : base(mainService)
        {
        }

        [HttpGet]
        public ApiResponse<IList<TransferenciaTipoDto>> Get()
        {
            return ProcessReponse(this._mainService.Get());
        }
    }
}
