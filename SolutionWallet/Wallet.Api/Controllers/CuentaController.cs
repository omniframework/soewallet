﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CuentaController : AppControllerBase<IServiceCuenta>
    {

        public CuentaController(
            IServiceCuenta serviceCuenta) : base(serviceCuenta)
        {
        }

        [HttpPost]
        [Route("my_accounts")]
        public ApiResponse<IList<CuentaDto>> GetMyAccounts([FromBody] GetMyAccountsParam param)
        {
            return ProcessReponse(this._mainService.GetMyAccounts(param));
        }
    }
}
