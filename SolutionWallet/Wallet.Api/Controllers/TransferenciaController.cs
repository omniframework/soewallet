﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransferenciaController : AppControllerBase<IServiceTransferencia>
    {
        public TransferenciaController(IServiceTransferencia mainService) : base(mainService)
        {
        }

        [HttpPost]        
        public ApiResponse<TransferenciaDto> Post([FromBody] PostTransferenciaParam param)
        {
            return ProcessReponse(this._mainService.PostStore(param));
        }
    }
}
