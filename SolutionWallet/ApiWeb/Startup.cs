using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiWeb.UnitOfWork;
using Generic.Api.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Security.Core;
using Security.Core.Impl;
using Security.Data.Repositories;
using Security.Data.Repositories.Impl;
using Security.Data.UnitOfWork;
using Wallet.Core;
using Wallet.Core.Impl;
using Wallet.Data.Repositories;
using Wallet.Data.Repositories.Impl;
using Wallet.Data.UnitOfWork;

namespace ApiWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add(typeof(GenericExcepcionFilter)))
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            Func<IServiceProvider, Context> contextFunc = (serviceProvider) =>
            {
                DbContextOptionsBuilder<Context> builder = new DbContextOptionsBuilder<Context>();
                string connectionString = Configuration.GetConnectionString("AppContext");
                builder.UseSqlServer(connectionString);
                return new Context(builder.Options);
            };
            services.AddScoped<ISecurityUnitOfWork, Context>(contextFunc);
            services.AddScoped<IWalletUnitOfWork, Context>(contextFunc);

            #region Repositories            
            services.AddScoped<IRepoUsuario, RepoUsuario>();

            services.AddScoped<IRepoCuenta, RepoCuenta>();
            services.AddScoped<IRepoMoneda, RepoMoneda>();
            services.AddScoped<IRepoTransaccion, RepoTransaccion>();
            services.AddScoped<IRepoTransaccionTipo, RepoTransaccionTipo>();
            services.AddScoped<IRepoTransferencia, RepoTransferencia>();
            services.AddScoped<IRepoTransferenciaTipo, RepoTransferenciaTipo>();
            services.AddScoped<IRepoCuentaAsociada, RepoCuentaAsociada>();

            #endregion

            #region Services           
            services.AddScoped<IServiceSession, ServiceSession>();
            services.AddScoped<IServiceUsuario, ServiceUsuario>();

            services.AddScoped<IServiceCuenta, ServiceCuenta>();
            services.AddScoped<IServiceMoneda, ServiceMoneda>();
            services.AddScoped<IServiceTransaccion, ServiceTransaccion>();
            services.AddScoped<IServiceTransferencia, ServiceTransferencia>();
            services.AddScoped<IServiceTransferenciaTipo, ServiceTransferenciaTipo>();
            services.AddScoped<IServiceCuentaAsociada, ServiceCuentaAsociada>();
            #endregion

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
