﻿using Generic.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core
{
    public interface IServiceTransferenciaTipo : IServiceBase
    {
        IList<TransferenciaTipoDto> Get();
    }
}
