﻿using Generic.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core
{
    public interface IServiceTransaccion : IServiceBase
    {
        TransaccionDto PostNewTransaccion(PostNewTransaccionParam param);
    }
}
