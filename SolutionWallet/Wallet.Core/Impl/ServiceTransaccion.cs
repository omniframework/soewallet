﻿using Generic.Core.Services.Impl;
using Generic.DataTransfer.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core.Resources;
using Wallet.Data.Entities;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core.Impl
{
    public class ServiceTransaccion : ServiceBase<IRepoTransaccion>, IServiceTransaccion
    {
        private IRepoMoneda _repoMoneda;
        private IRepoCuenta _repoCuenta;
        private IRepoTransaccionTipo _repoTransaccionTipo;

        public ServiceTransaccion(
            IRepoTransaccionTipo repoTransaccionTipo,
            IRepoMoneda repoMoneda,
            IRepoTransaccion mainRepo,
            IRepoCuenta repoCuenta) : base(mainRepo)
        {
            this._repoMoneda = repoMoneda;
            this._repoCuenta = repoCuenta;
            this._repoTransaccionTipo = repoTransaccionTipo;
        }

        public TransaccionDto PostNewTransaccion(PostNewTransaccionParam param)
        {
            CuentaDto cuenta = this._repoCuenta.GetBy(param.CuentaId);
            TransaccionTipoDto transaccionTipo = this._repoTransaccionTipo.GetBy(param.TransaccionTipoId);
            MonedaDto moneda = this._repoMoneda.Get(param.MonedaId);
            decimal? equivalencia = null;
            if (cuenta != null)
            {
                if (param.MonedaId != cuenta.MonedaId)
                {
                    equivalencia = this._repoMoneda.GetTipoCambio(param.MonedaId, cuenta.MonedaId);
                }
                else
                {
                    equivalencia = 1;
                }
            }
            #region Validaciones
            ProcessValidations((messages) =>
            {
                if (cuenta == null)
                {
                    messages.Add(string.Format(ServiceTransaccionMessage.AccountNotFound, param.CuentaId));
                }

                if (transaccionTipo == null)
                {
                    messages.Add(string.Format(ServiceTransaccionMessage.TransaccionTypeNotFound, param.TransaccionTipoId));
                }

                if (moneda == null)
                {
                    messages.Add(string.Format(ServiceTransaccionMessage.CurrencyNotFound, param.MonedaId));
                }

                if (param.MontoOriginal <= 0)
                {
                    messages.Add(ServiceTransaccionMessage.AmountGeCero);
                }

                if (messages.Count == 0)
                {
                    if (!equivalencia.HasValue)
                    {
                        messages.Add(string.Format(ServiceTransaccionMessage.CurrencyEqNotFound, moneda.Name, cuenta.MonedaName));
                    }

                    if (cuenta.Balance <= 0 && !transaccionTipo.Ingreso)
                    {
                        messages.Add(ServiceTransaccionMessage.NoBalanceFound);
                    }
                }
            });
            #endregion
            decimal monto = param.MontoOriginal;
            decimal tipo = transaccionTipo.Ingreso ? 1 : -1;
            if (equivalencia.Value != 1)
            {
                monto *= equivalencia.Value;
            }
            monto *= tipo;
            Transaccion transaccion = new Transaccion()
            {
                Concepto = param.Concepto,
                CuentaId = param.CuentaId,
                Descripcion = param.Descripcion,
                Estado = StateEnum.Enabled,
                FechaOcurrencia = DateTime.Now,
                MonedaCuentaId = cuenta.MonedaId,
                FechaAnulacion = null,
                MotivoAnulacion = null,
                FechaCreacion = DateTime.Now,
                FechaEdicion = DateTime.Now,
                MonedaOriginalId = param.MonedaId,
                MontoOriginal = param.MontoOriginal,
                TipoCambioValor = equivalencia.Value,
                TransaccionTipoId = transaccionTipo.TransaccionTipoId,
                UsuarioCreacion = null,
                UsuarioEdicion = null,
                Monto = monto,
            };            
            this._mainRepo.PostStore(transaccion);
            this._mainRepo.SaveChanges();

            this._mainRepo.UpdateAccountBalance(cuenta.CuentaId);
            this._mainRepo.SaveChanges();

            this._mainRepo.UpdateBilleteraBalance(cuenta.UsuarioId);
            this._mainRepo.SaveChanges();

            return this._mainRepo.Get(transaccion.TransaccionId);
        }
    }
}
