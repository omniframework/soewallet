﻿using Generic.Core.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core.Impl
{
    public class ServiceTransferenciaTipo : ServiceBase<IRepoTransferenciaTipo>, IServiceTransferenciaTipo
    {
        public ServiceTransferenciaTipo(IRepoTransferenciaTipo mainRepo) : base(mainRepo)
        {
        }

        public IList<TransferenciaTipoDto> Get()
        {
            return this._mainRepo.Get();
        }
    }
}
