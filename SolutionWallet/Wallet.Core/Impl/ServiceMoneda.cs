﻿using Generic.Core.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core.Impl
{
    public class ServiceMoneda : ServiceBase<IRepoMoneda>, IServiceMoneda
    {
        public ServiceMoneda(IRepoMoneda mainRepo) : base(mainRepo)
        {
        }

        public IList<MonedaDto> Get()
        {
            return this._mainRepo.Get();
        }
    }
}
