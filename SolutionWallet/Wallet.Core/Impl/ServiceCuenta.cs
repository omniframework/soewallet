﻿using Generic.Core.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core.Impl
{
    public class ServiceCuenta : ServiceBase<IRepoCuenta>, IServiceCuenta
    {
        public ServiceCuenta(IRepoCuenta repoCuenta): base(repoCuenta)
        {
        }

        public IList<CuentaDto> GetMyAccounts(GetMyAccountsParam param)
        {
            return this._mainRepo.GetMyAccounts(param);
        }
    }
}
