﻿using Generic.Core.Services.Impl;
using Generic.DataTransfer.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Core.Resources;
using Wallet.Data.Entities;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;
using Wallet.Entities.Enums;

namespace Wallet.Core.Impl
{
    public class ServiceTransferencia : ServiceBase<IRepoTransferencia>, IServiceTransferencia
    {
        private IServiceTransaccion _serviceTransaction;

        private IRepoMoneda _repoMoneda;
        private IRepoCuenta _repoCuenta;
        private IRepoTransferenciaTipo _repoTransferenciaTipo;

        public ServiceTransferencia(
            IServiceTransaccion serviceTransaction,
            IRepoTransferenciaTipo repoTransferenciaTipo,
            IRepoCuenta repoCuenta,
            IRepoMoneda repoMoneda,
            IRepoTransferencia mainRepo) : base(mainRepo)
        {
            this._serviceTransaction = serviceTransaction;
            this._repoCuenta = repoCuenta;
            this._repoMoneda = repoMoneda;
            this._repoTransferenciaTipo = repoTransferenciaTipo;
        }

        public TransferenciaDto PostStore(PostTransferenciaParam param)
        {
            CuentaDto cuentaOrigen = this._repoCuenta.GetBy(param.CuentaOrigenId);
            CuentaDto cuentaDestino = this._repoCuenta.GetBy(param.CuentaDestinoId);
            decimal? equivalencia = null;
            if (cuentaOrigen != null && cuentaDestino != null)
            {
                if (cuentaOrigen.MonedaId != cuentaDestino.MonedaId)
                {
                    equivalencia = this._repoMoneda.GetTipoCambio(cuentaOrigen.MonedaId, cuentaDestino.MonedaId);
                }
                else
                {
                    equivalencia = 1;
                }
            }
            #region Validaciones
            ProcessValidations((messages) =>
            {
                if (!this._repoTransferenciaTipo.Exists(param.TransferenciaTipoId))
                {
                    messages.Add(string.Format(ServiceTransferenciaMessage.TransferenciaTypeNotFound, param.TransferenciaTipoId));
                }
                if (cuentaOrigen == null)
                {
                    messages.Add(string.Format(ServiceTransaccionMessage.AccountNotFound, param.CuentaOrigenId));
                }
                if (cuentaDestino == null)
                {
                    messages.Add(string.Format(ServiceTransaccionMessage.AccountNotFound, param.CuentaDestinoId));
                }
                if (param.Monto <= 0)
                {
                    messages.Add(ServiceTransaccionMessage.AmountGeCero);
                }
                if (messages.Count == 0)
                {
                    if (!equivalencia.HasValue)
                    {
                        messages.Add(string.Format(ServiceTransaccionMessage.CurrencyEqNotFound, cuentaOrigen.MonedaName, cuentaDestino.MonedaName));
                    }

                    if ((cuentaOrigen.Balance - param.Monto) < 0)
                    {
                        messages.Add(ServiceTransaccionMessage.NoBalanceFound);
                    }
                }
            });
            #endregion            
            PostNewTransaccionParam postNewOrigin = new PostNewTransaccionParam()
            {
                Concepto = param.Concepto,
                CuentaId = param.CuentaOrigenId,
                Descripcion = param.Descripcion,
                MonedaId = cuentaOrigen.MonedaId,
                MontoOriginal = param.Monto,
                TransaccionTipoId = TransaccionTipoEnum.TransferenciaEgreso
            };

            PostNewTransaccionParam postNewDest = new PostNewTransaccionParam()
            {
                Concepto = param.Concepto,
                CuentaId = param.CuentaDestinoId,
                Descripcion = param.Descripcion,
                MonedaId = cuentaOrigen.MonedaId,
                MontoOriginal = param.Monto,
                TransaccionTipoId = TransaccionTipoEnum.TransferenciaIngreso
            };

            TransaccionDto transaccionOrigen = this._serviceTransaction.PostNewTransaccion(postNewOrigin);
            TransaccionDto transaccionDest = this._serviceTransaction.PostNewTransaccion(postNewDest);
            
            Transferencia transferencia = new Transferencia()
            {
                Concepto = param.Concepto,
                Descripcion = param.Descripcion,
                Estado = StateEnum.Enabled,
                CuentaDestinoId = cuentaDestino.CuentaId,
                CuentaOrigenId = cuentaOrigen.CuentaId,
                FechaAnulacion = null,
                MotivoAnulacion = null,
                FechaOcurrencia = DateTime.Now,
                MonedaDestinoId = cuentaDestino.MonedaId,
                MonedaOrigenId = cuentaOrigen.MonedaId,
                TipoCambioValor = equivalencia.Value,
                MontoOriginal = param.Monto,
                Monto = transaccionDest.Monto,
                TransaccionDestinoId = transaccionDest.TransaccionId,
                TransaccionOrigenId = transaccionOrigen.TransaccionId,
                TransferenciaTipoId = param.TransferenciaTipoId,
                UsuarioCreacion = null,
                UsuarioEdicion = null,                 
                FechaCreacion = DateTime.Now,
                FechaEdicion = DateTime.Now,
            };

            this._mainRepo.PostStore(transferencia);
            this._mainRepo.SaveChanges();

            return this._mainRepo.Get(transferencia.TransferenciaId);
        }
    }
}
