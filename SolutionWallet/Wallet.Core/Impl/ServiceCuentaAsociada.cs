﻿using Generic.Core.Services.Impl;
using System;
using System.Collections.Generic;
using System.Text;
using Wallet.Data.Repositories;
using Wallet.Entities.DataTransfers;
using Wallet.Entities.DataViews;

namespace Wallet.Core.Impl
{
    public class ServiceCuentaAsociada : ServiceBase<IRepoCuentaAsociada>, IServiceCuentaAsociada
    {
        public ServiceCuentaAsociada(IRepoCuentaAsociada mainRepo) : base(mainRepo)
        {
        }

        public IList<CuentaAsociadaDto> GetFavoriteAccount(GetFavoriteAccountsParam param)
        {
            return this._mainRepo.GetFavoriteAccount(param);
        }
    }
}
