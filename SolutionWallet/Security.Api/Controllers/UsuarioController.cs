﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Microsoft.AspNetCore.Mvc;
using Security.Core;
using Security.Entities.DataTransfers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : AppControllerBase<IServiceUsuario>
    {
        public UsuarioController(IServiceUsuario mainService) : base(mainService)
        {
        }

        [HttpGet("{id}", Name = "Get")]
        public ApiResponse<UsuarioDto> Get(long id)
        {
            return ProcessReponse(this._mainService.Get(id));
        }
    }
}
