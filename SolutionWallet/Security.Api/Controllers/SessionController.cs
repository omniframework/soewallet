﻿using Generic.Api.Controllers;
using Generic.DataTransfer;
using Generic.DataTransfer.Enums;
using Microsoft.AspNetCore.Mvc;
using Security.Core;
using Security.Entities;
using Security.Entities.DataTransfers;
using Security.Entities.Params;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SessionController : AppControllerBase<IServiceSession>
    {
        public SessionController(IServiceSession mainService) : base(mainService)
        {
        }

        [HttpPost]
        [Route("login")]
        public ApiResponse<UsuarioDto> Login([FromBody] LoginParam param)
        {
            return ProcessReponse(this._mainService.Login(param));
        }

        [HttpPost]
        [Route("change_password")]
        public ApiResponse<UsuarioDto> ChangePassword([FromBody] ChangePasswordParam param)
        {
            return ProcessReponse(this._mainService.ChangePassword(param));
        }

        [HttpPost]
        [Route("close_session")]
        public ApiResponse<bool> CloseSession([FromBody] CloseSessionParam param)
        {
            return ProcessReponse(true);
        }
    }
}
