﻿using Generic.Data.Repositories;
using Security.Entities.DataTransfers;
using Security.Entities.Params;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Data.Repositories
{
    public interface IRepoUsuario : IRepoBase
    {
        UsuarioDto Get(long usuarioId);

        UsuarioDto GetBy(LoginParam param);
        UsuarioDto GetBy(long usuarioId, string password);
        void ChangePassword(ChangePasswordParam param);
    }
}
