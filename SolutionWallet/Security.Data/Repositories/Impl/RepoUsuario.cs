﻿using Generic.Data.Repositories.Impl;
using Security.Data.UnitOfWork;
using Security.Entities.DataTransfers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Security.Entities.Params;
using Security.Data.Entities;
using Generic.DataTransfer.Enums;

namespace Security.Data.Repositories.Impl
{
    public class RepoUsuario : RepoBase<ISecurityUnitOfWork>, IRepoUsuario
    {
        public RepoUsuario(ISecurityUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public void ChangePassword(ChangePasswordParam param)
        {
            Usuario usuario = (from u in this.UnitOfWork.Usuario
                               where u.UsuarioId == param.UsuarioId
                               && u.Estado != StateEnum.Deleted
                               select u).FirstOrDefault();

            usuario.Password = param.NewPassword;
            usuario.FechaEdicion = DateTime.Now;
        }

        public UsuarioDto Get(long usuarioId)
        {
            return (from u in this.UnitOfWork.Usuario
                    where u.UsuarioId == usuarioId
                    && u.Estado != StateEnum.Deleted
                    select new UsuarioDto()
                    {
                        UsuarioId = u.UsuarioId,
                        CorreoElectronico = u.CorreoElectronico,
                        Estado = u.Estado,
                        NickName = u.NickName,
                        NombreCompleto = u.NombreCompleto,
                        PrimerInicio = u.PrimerInicio,
                        PrimerNombre = u.PrimerNombre,
                        SegundoNombre = u.SegundoNombre,
                        Telefono = u.Telefono
                    }).FirstOrDefault();
        }

        public UsuarioDto GetBy(LoginParam param)
        {
            return (from u in this.UnitOfWork.Usuario
                    where u.NickName == param.NickName
                    && u.Password == param.Password
                    && u.Estado != StateEnum.Deleted
                    select new UsuarioDto()
                    {
                        UsuarioId = u.UsuarioId,
                        CorreoElectronico = u.CorreoElectronico,
                        Estado = u.Estado,
                        NickName = u.NickName,
                        NombreCompleto = u.NombreCompleto,
                        PrimerInicio = u.PrimerInicio,
                        PrimerNombre = u.PrimerNombre,
                        SegundoNombre = u.SegundoNombre,
                        Telefono = u.Telefono
                    }).FirstOrDefault();
        }

        public UsuarioDto GetBy(long usuarioId, string password)
        {
            return (from u in this.UnitOfWork.Usuario
                    where u.UsuarioId == usuarioId
                    && u.Password == password
                    && u.Estado != StateEnum.Deleted
                    select new UsuarioDto()
                    {
                        UsuarioId = u.UsuarioId,
                        CorreoElectronico = u.CorreoElectronico,
                        Estado = u.Estado,
                        NickName = u.NickName,
                        NombreCompleto = u.NombreCompleto,
                        PrimerInicio = u.PrimerInicio,
                        PrimerNombre = u.PrimerNombre,
                        SegundoNombre = u.SegundoNombre,
                        Telefono = u.Telefono
                    }).FirstOrDefault();
        }
    }
}
