﻿using Generic.Data.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Security.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Data.UnitOfWork
{
    public interface ISecurityUnitOfWork : IUnitOfWorkBase
    {
        DbSet<Usuario> Usuario { get; set; }
    }
}
