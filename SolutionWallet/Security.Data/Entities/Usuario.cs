﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Data.Entities
{
    public class Usuario
    {
        public long UsuarioId { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }
        public string NombreCompleto { get; set; }
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string Telefono { get; set; }
        public string CorreoElectronico { get; set; }
        public bool PrimerInicio { get; set; }
        public short Estado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaEdicion { get; set; }
        public long? UsuarioCreacion { get; set; }
        public long? UsuarioEdicion { get; set; }
    }
}
