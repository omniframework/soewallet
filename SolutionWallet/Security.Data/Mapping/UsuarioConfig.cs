﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Security.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Security.Data.Mapping
{
    public class UsuarioConfig : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder
                .HasKey(c => c.UsuarioId);

            builder.Property(c => c.UsuarioId)
                .ValueGeneratedNever();


            builder.Property(c => c.NickName)
                .HasMaxLength(50)
                .IsRequired(true);

            builder.Property(c => c.Telefono)
                .HasMaxLength(20)
                .IsRequired(false);

            builder.Property(c => c.CorreoElectronico)
                .HasMaxLength(100)
                .IsRequired(true);

            builder.Property(c => c.NombreCompleto)
                .HasMaxLength(305)
                .IsRequired(true);

            builder.Property(c => c.PrimerNombre)
                .HasMaxLength(150)
                .IsRequired(true);

            builder.Property(c => c.SegundoNombre)
                .HasMaxLength(150)
                .IsRequired(false);

            builder.Property(c => c.Password)
                .HasMaxLength(150)
                .IsRequired(true);

            builder.Property(c => c.PrimerInicio)
                .IsRequired(true);

            builder.Property(c => c.Estado)
                .IsRequired(true);

            builder.Property(c => c.UsuarioCreacion)
                .IsRequired(false);

            builder.Property(c => c.UsuarioEdicion)
                .IsRequired(false);

            builder.Property(c => c.FechaCreacion)
                .IsRequired(true)
                .HasColumnType(string.Format($"datetime"));

            builder.Property(c => c.FechaEdicion)
                .IsRequired(false)
                .HasColumnType(string.Format($"datetime"));            
        }
    }
}
