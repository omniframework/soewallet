﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Generic.DataTransfer.Exceptions
{
    public class ValidationException : Exception
    {
        public List<string> Errores { get; set; }

        public ValidationException(string message, params string[] errors) : base(message)
        {
            this.Errores = errors.ToList();
        }
    }
}
