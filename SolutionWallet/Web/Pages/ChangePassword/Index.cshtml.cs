﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Models;
using Web.Service;

namespace Web.Pages.ChangePassword
{
    public class IndexModel : PageModel
    {
        IConsultaApiWallet service;

        [BindProperty]
        public Web.Models.ChangePassword ChangePassword { get; set; }

        public string Mensaje { get; set; }
        [BindProperty]
        private long UsuarioId { get; set; }


        public IndexModel(IConsultaApiWallet service)
        {
            this.service = service;
        }

        public void OnGet()
        {
            UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId"));
        }

        public IActionResult OnPostChangePassword()
        {
            if (string.IsNullOrWhiteSpace(ChangePassword.ContraseñaActual))
            {
                Mensaje = Web.Helper.Configuration.REQUIERE_CONTRASEÑA_ACTUAL;
                return Page();
            }

            if (string.IsNullOrWhiteSpace(ChangePassword.NuevaContraseña))
            {
                Mensaje = Web.Helper.Configuration.REQUIERE_NUEVA_CONTRASEÑA;
                return Page();
            }

            if (string.IsNullOrWhiteSpace(ChangePassword.ConfirmarNuevaContraseña))
            {
                Mensaje = Web.Helper.Configuration.REQUIERE_CONFIRMAR_NUEVA_CONTRASEÑA;
                return Page();
            }

            if (ChangePassword.NuevaContraseña != ChangePassword.ConfirmarNuevaContraseña) 
            {
                Mensaje = Web.Helper.Configuration.MSJ_NO_COINCIDE_CONTRASENA;
                return Page();
            }

            var res = service.ChangePassword(new Service.Param.ChangePasswordParam()
            {
                UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId")),
                Password = Web.Helper.Helper.CalculateMD5Hash(ChangePassword.ContraseñaActual),
                NewPassword = Web.Helper.Helper.CalculateMD5Hash(ChangePassword.NuevaContraseña),
            });

            if (res.codigo == 0)
            {
                return RedirectToPage("../Index");
            }
            else
            {
                if (res.errores.Count > 0)
                {
                    var respuesta = string.Empty;
                    foreach (var msj in res.errores)
                    {
                        respuesta = respuesta + msj + ". ";
                    }
                    Mensaje = res.mensaje + " - " + respuesta;
                }

                return Page();
            }

        }
    }
}