﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Models;
using Web.Service;

namespace Web.Pages
{
    public class IndexModel : PageModel
    {
        IConsultaApiWallet service;

        [BindProperty]
        public Login Login { get; set; }

        public string Mensaje { get; set; }

        public IndexModel(IConsultaApiWallet service)
        {
            this.service = service;
        }
        public void OnGet()
        {

        }

        public IActionResult OnPostLogin() 
        {
            if (string.IsNullOrWhiteSpace(Login.Usuario)) 
            {
                Mensaje = Web.Helper.Configuration.REQUIERE_USUARIO;
                return Page();
            }

            if (string.IsNullOrWhiteSpace(Login.Contraseña)) 
            {
                Mensaje = Web.Helper.Configuration.REQUIERE_CONTRASEÑA;
                return Page();
            }

            var res = service.Login(new Service.Param.LoginParam()
            {
                NickName = Login.Usuario,
                Password = Web.Helper.Helper.CalculateMD5Hash(Login.Contraseña)
            }) ;

            if (res.codigo == 0)
            {   
                HttpContext.Session.SetString("UsuarioId", res.data.UsuarioId.ToString());
                return RedirectToPage("Account/Index");
            }
            else 
            {
                if (res.errores.Count > 0) 
                {
                    var respuesta = string.Empty;
                    foreach (var msj in res.errores)
                    {
                        respuesta = respuesta + msj + ". ";
                    }
                    Mensaje = res.mensaje + " - " + respuesta;
                }
                
                return Page();
            }
          
        }
    }
}