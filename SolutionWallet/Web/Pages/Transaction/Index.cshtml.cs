﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Web.Models;
using Web.Service;
using Web.Service.Dtos;

namespace Web.Pages.Transaction
{
    public class IndexModel : PageModel
    {
        IConsultaApiWallet service;

        public List<SelectListItem> SelectMyAccounts { get; set; }

        public List<SelectListItem> SelectTypeTrans { get; set; }

        public List<SelectListItem> SelectAccountDesti { get; set; }

        public List<SelectListItem> SelectMonedas { get; set; }
        [BindProperty]
        public Web.Models.Transaction Transaccion { get; set; }

        public long UsuarioId { get; set; }

        public string Mensaje { get; set; }

        public IndexModel(IConsultaApiWallet service)
        {
            this.service = service;
        }

        public void OnGet()
        {
            UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId"));

            this.SelectMyAccounts = GetMyAccount(UsuarioId);

            this.SelectTypeTrans = GetTypeTransferencia();

            this.SelectAccountDesti = GetAccountDestination(UsuarioId);

            this.SelectMonedas = GetMonedas();
        }

        public List<SelectListItem> GetMyAccount(long usuarioId)
        {
            var result = new List<SelectListItem>();

            ResultElement<List<CuentaDto>> res = service.GetMyAccounts(new Service.Param.GetMyAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0)
            {
                foreach (var cuenta in res.data)
                {
                    var item = new SelectListItem()
                    {
                        Value = cuenta.CuentaId.ToString(),
                        Text = cuenta.CuentaTipoName + " - " + cuenta.Balance + " - " + cuenta.MonedaAbreb
                    };
                    result.Add(item);
                }
            }

            return result;
        }

        public List<SelectListItem> GetTypeTransferencia()
        {
            var result = new List<SelectListItem>();

            ResultElement<List<TransferenciaTipoDto>> res = service.GetTypeTransferencia();

            if (res.codigo == 0)
            {
                foreach (var typeTransfe in res.data)
                {
                    var item = new SelectListItem()
                    {
                        Value = typeTransfe.TransferenciaTipoId.ToString(),
                        Text = typeTransfe.Nombre
                    };
                    result.Add(item);
                }
            }

            return result;
        }

        public List<SelectListItem> GetAccountDestination(long usuarioId)
        {
            var result = new List<SelectListItem>();

            ResultElement<List<CuentaAsociadaDto>> res = service.GetFavoriteAccount(new Service.Param.GetFavoriteAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0)
            {
                foreach (var cuenta in res.data)
                {
                    var item = new SelectListItem()
                    {
                        Value = cuenta.CuentaId.ToString(),
                        Text = cuenta.CuentaTipoName + " - " + cuenta.Alias + " - " + cuenta.MonedaCuentaAbreb
                    };
                    result.Add(item);
                }
            }

            return result;
        }

        public List<SelectListItem> GetMonedas()
        {
            var result = new List<SelectListItem>();

            ResultElement<List<MonedaDto>> res = service.GetMoneda();

            if (res.codigo == 0)
            {
                foreach (var moneda in res.data)
                {
                    var item = new SelectListItem()
                    {
                        Value = moneda.MonedaId.ToString(),
                        Text = moneda.Descripcion
                    };
                    result.Add(item);
                }
            }

            return result;
        }

        public IActionResult OnPostTransaction()
        {
            var res = service.Transaccion(new Service.Param.PostTransferenciaParam()
            {
                CuentaOrigenId  = Transaccion.MyAccountId,
                CuentaDestinoId = Transaccion.AccountDestiId,
                Monto = Transaccion.Monto,
                Descripcion = Transaccion.Descripcion,
                TransferenciaTipoId = Transaccion.TypeTransferenciaId
            });

            if (res.codigo == 0)
            {
                return RedirectToPage("/Account/Index");
            }
            else
            {
                if (res.errores.Count > 0)
                {
                    var respuesta = string.Empty;
                    foreach (var msj in res.errores)
                    {
                        respuesta = respuesta + msj + ". ";
                    }
                    Mensaje = res.mensaje + " - " + respuesta;
                }

                return Page();
            }

        }
    }
}