﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Service;
using Web.Service.Dtos;

namespace Web.Pages.Account
{
    public class DetailsMyAccountModel : PageModel
    {
        IConsultaApiWallet service;
        public Models.MyAccount MyAccounts { get; set; }

        public long UsuarioId { get; set; }

        public DetailsMyAccountModel(IConsultaApiWallet service)
        {
            this.service = service;
        }
        public void OnGet(long ID)
        {
            UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId"));

            this.MyAccounts = GetMyAccount(UsuarioId,ID);
        }
        public Models.MyAccount GetMyAccount(long usuarioId, long ID)
        {
            var result = new Models.MyAccount();

            ResultElement<List<CuentaDto>> res = service.GetMyAccounts(new Service.Param.GetMyAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0)
            {
                var  cuenta = res.data.Find(p => p.CuentaId == ID);

                result = new Models.MyAccount() 
                {
                    CuentaId = cuenta.CuentaId,
                    Balance = cuenta.Balance,
                    CuentaTipoId = cuenta.CuentaTipoId,
                    CuentaTipoName = cuenta.CuentaTipoName,
                    Estado = cuenta.Estado,
                    MonedaAbreb = cuenta.MonedaAbreb,
                    MonedaId= cuenta.MonedaId,
                    MonedaName= cuenta.MonedaName,
                    UsuarioId   = cuenta.UsuarioId
                };

               
            }

            return result;
        }
    }
}