﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Models;
using Web.Service;
using Web.Service.Dtos;

namespace Web.Pages.Account
{
    public class IndexModel : PageModel
    {
        IConsultaApiWallet service;

        public List<Models.MyAccount> MyAccounts { get; set; }
        public List<Models.FavoriteAccount> favoriteAccounts { get; set; }

        public long UsuarioId { get; set; }

        public string Mensaje { get; set; }


        public IndexModel(IConsultaApiWallet service)
        {
            this.service = service;
        }

        public void OnGet()
        {
            UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId"));

            this.MyAccounts = GetMyAccount(UsuarioId);

            this.favoriteAccounts = GetFavoriteAccount(UsuarioId);
        }

        public List<Models.MyAccount> GetMyAccount(long usuarioId)
        {
            var result = new List<Models.MyAccount>();

            ResultElement<List<CuentaDto>> res = service.GetMyAccounts(new Service.Param.GetMyAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0) 
            {
                foreach (var cuenta in res.data)
                {
                    var myCuenta = new Models.MyAccount()
                    {   
                       CuentaId = cuenta.CuentaId,
                       Balance = cuenta.Balance,
                       CuentaTipoName = cuenta.CuentaTipoName,
                       MonedaName = cuenta.MonedaName
                    };
                    result.Add(myCuenta);
                }
            }

            return result;
        }

        public List<Models.FavoriteAccount> GetFavoriteAccount(long usuarioId) 
        {
            var result = new List<Models.FavoriteAccount>();

            ResultElement<List<CuentaAsociadaDto>> res = service.GetFavoriteAccount(new Service.Param.GetFavoriteAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0)
            {
                foreach (var cuenta in res.data)
                {
                    var cuentaFavorita = new Models.FavoriteAccount()
                    {
                        CuentaId = cuenta.CuentaId,
                        Alias = cuenta.Alias,
                        CuentaTipoName = cuenta.CuentaTipoName,
                        MonedaCuentaName = cuenta.MonedaCuentaName
                    };
                    result.Add(cuentaFavorita);
                }
            }

            return result;
        }
        public IActionResult OnGetLogout()
        {
            var res = service.CloseSession(new Service.Param.CloseSessionParam()
            {
                UsuarioId = UsuarioId
            });

            if (res.codigo == 0)
            {
                return RedirectToPage("../Index");
            }
            else
            {
                if (res.errores.Count > 0)
                {
                    var respuesta = string.Empty;
                    foreach (var msj in res.errores)
                    {
                        respuesta = respuesta + msj + ". ";
                    }
                    Mensaje = res.mensaje + " - " + respuesta;
                }

                return Page();
            }
        }
     
    }
}