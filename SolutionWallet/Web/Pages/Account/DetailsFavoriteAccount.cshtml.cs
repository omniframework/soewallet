﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Service;
using Web.Service.Dtos;

namespace Web.Pages.Account
{
    public class DetailsFavoriteAccountModel : PageModel
    {
        IConsultaApiWallet service;
        public Models.FavoriteAccount FavoriteAccounts { get; set; }

        public long UsuarioId { get; set; }

        public DetailsFavoriteAccountModel(IConsultaApiWallet service)
        {
            this.service = service;
        }
        public void OnGet(long ID)
        {
            UsuarioId = Convert.ToInt64(HttpContext.Session.GetString("UsuarioId"));

            this.FavoriteAccounts = GetFavoriteAccount(UsuarioId, ID);
        }

        public Models.FavoriteAccount GetFavoriteAccount(long usuarioId, long ID)
        {
            var result = new Models.FavoriteAccount();

            ResultElement<List<CuentaAsociadaDto>> res = service.GetFavoriteAccount(new Service.Param.GetFavoriteAccountsParam()
            {
                UsuarioId = usuarioId
            });

            if (res.codigo == 0)
            {
                var cuenta = res.data.Find(p => p.CuentaId == ID);

                result = new Models.FavoriteAccount()
                {
                    CuentaId = cuenta.CuentaId,
                    Alias = cuenta.Alias,
                    CuentaTipoName = cuenta.CuentaTipoName,
                    MonedaCuentaName = cuenta.MonedaCuentaName,
                    CuentaAsociadaId = cuenta.CuentaAsociadaId,
                    CuentaTipoId = cuenta.CuentaTipoId,
                    MonedaCuentaAbreb = cuenta.MonedaCuentaAbreb,
                    MonedaCuentaId = cuenta.MonedaCuentaId,
                    UsuarioId = cuenta.UsuarioId
                };


            }

            return result;
        }
    }
}