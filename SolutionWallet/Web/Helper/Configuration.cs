﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Helper
{
    public class Configuration
    {
        public static string URL_API = "https://192.168.43.169/WalletApi/";
        public static string MSJ_NO_COINCIDE_CONTRASENA = "No coincide las nuevas contraseña ingresadas";

        public static string REQUIERE_USUARIO = "Requiere ingresar usuario";
        public static string REQUIERE_CONTRASEÑA = "Requiere ingresar contraseña";

        public static string REQUIERE_CONTRASEÑA_ACTUAL = "Requiere ingresar contraseña actual";
        public static string REQUIERE_NUEVA_CONTRASEÑA = "Requiere ingresar nueva contraseña";
        public static string REQUIERE_CONFIRMAR_NUEVA_CONTRASEÑA = "Requiere ingresar confirmar nueva contraseña";



    }
}
