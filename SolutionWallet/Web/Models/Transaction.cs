﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class Transaction
    {
        public long UsuarioId { get; set; }

        public long MyAccountId { get; set; }

        public short TypeTransferenciaId { get; set; }

        public long AccountDestiId { get; set; }

        public long MonedaId { get; set; }

        public decimal Monto { get; set; }

        public string Descripcion { get; set; }
    }
}
