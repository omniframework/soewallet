﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class FavoriteAccount
    {
        public long CuentaAsociadaId { get; set; }
        public long CuentaId { get; set; }
        public long UsuarioId { get; set; }
        public string Alias { get; set; }
        public short CuentaTipoId { get; set; }
        public short MonedaCuentaId { get; set; }
        public string CuentaTipoName { get; set; }
        public string MonedaCuentaAbreb { get; set; }
        public string MonedaCuentaName { get; set; }
    }
}
