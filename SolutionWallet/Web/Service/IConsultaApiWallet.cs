﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Service.Dtos;
using Web.Service.Param;

namespace Web.Service
{
    public interface IConsultaApiWallet
    {
        public ResultElement<List<CuentaDto>> GetMyAccounts(GetMyAccountsParam param);

        public ResultElement<List<CuentaAsociadaDto>> GetFavoriteAccount(GetFavoriteAccountsParam param);

        public ResultElement<UsuarioDto> Login(LoginParam param);

        public ResultElement<UsuarioDto> ChangePassword(ChangePasswordParam param);

        public ResultElement<bool> CloseSession(CloseSessionParam param);

        public ResultElement<List<MonedaDto>> GetMoneda();

        public ResultElement<TransferenciaDto> Transaccion(PostTransferenciaParam param);

        public ResultElement<List<TransferenciaTipoDto>> GetTypeTransferencia();


    }
}
