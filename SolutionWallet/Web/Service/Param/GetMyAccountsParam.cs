﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service.Param
{
    public class GetMyAccountsParam
    {
        public long UsuarioId { get; set; }
    }
}
