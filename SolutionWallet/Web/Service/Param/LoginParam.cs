﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service.Param
{
    public class LoginParam
    {
        public string NickName { get; set; }
        public string Password { get; set; }
    }
}
