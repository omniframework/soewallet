﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service.Param
{
    public class ChangePasswordParam
    {
        public long UsuarioId { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
    }
}
