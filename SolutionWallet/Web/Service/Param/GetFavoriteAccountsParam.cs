﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service.Param
{
    public class GetFavoriteAccountsParam
    {
        public long UsuarioId { get; set; }
    }
}
