﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service
{
    public class ApiResponse<T>
    {
        public int codigo { get; set; }
        public List<T> data { get; set; }
        public string mensaje { get; set; }
        public List<string> errores { get; set; }


    }
    public class ApiResponseObject<T>
    {
        public int codigo { get; set; }
        public T data { get; set; }
        public string mensaje { get; set; }
        public List<string> errores { get; set; }


    }
}
