﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service
{
    public class ResultElement<T>
    {
        public int codigo { get; set; }
        public T data { get; set; }
        public string mensaje { get; set; }
        public List<string> errores { get; set; }
    }
}
