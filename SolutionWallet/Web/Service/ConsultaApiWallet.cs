﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Helper;
using Web.Service.Dtos;
using Web.Service.Param;

namespace Web.Service
{
    public class ConsultaApiWallet: IConsultaApiWallet
    {
        public ResultElement<List<CuentaDto>> GetMyAccounts(GetMyAccountsParam param)
        {
            var result = new ResultElement<List<CuentaDto>>();
            try
            {
                #region Proceso

                var paramService = param;
                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}cuenta/my_accounts", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.POST);
                var serializeObject = JsonConvert.SerializeObject(paramService);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponse<CuentaDto>>(response.Content);

                        result = new ResultElement<List<CuentaDto>>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };

                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<List<CuentaAsociadaDto>> GetFavoriteAccount(GetFavoriteAccountsParam param) 
        {
            var result = new ResultElement<List<CuentaAsociadaDto>>();
            try
            {
                #region Proceso

                    var paramService = param;
                    var urlWsBase =Configuration.URL_API;
                    var sUrl = string.Format("{0}cuenta_asociada/favorite_account", urlWsBase);
                    var client = new RestClient(sUrl);
                    var request = new RestRequest(Method.POST);
                    var serializeObject = JsonConvert.SerializeObject(paramService);
                    request.AddHeader("content-type", "application/json");
                    request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                    var response = client.Execute(request);
                    var status = response.StatusCode;

                    switch (status)
                    {
                        case System.Net.HttpStatusCode.OK:
                            var content = JsonConvert.DeserializeObject<ApiResponse<CuentaAsociadaDto>>(response.Content);

                            result = new ResultElement<List<CuentaAsociadaDto>>()
                            {
                                codigo = content.codigo,
                                data = content.data,
                                errores = content.errores,
                                mensaje= content.mensaje
                            };
    
                        break;
                        default:
                            break;
                    }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }
        

        public ResultElement<UsuarioDto> Login(LoginParam param) 
        {
            var result = new ResultElement<UsuarioDto>();
            try
            {
                #region Proceso

                var paramService = param;
                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}Session/login", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.POST);
                var serializeObject = JsonConvert.SerializeObject(paramService);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponseObject<UsuarioDto>>(response.Content);

                        result = new ResultElement<UsuarioDto>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje= content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<UsuarioDto> ChangePassword(ChangePasswordParam param) 
        {
            var result = new ResultElement<UsuarioDto>();
            try
            {
                #region Proceso

                var paramService = param;
                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}Session/change_password", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.POST);
                var serializeObject = JsonConvert.SerializeObject(paramService);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponseObject<UsuarioDto>>(response.Content);

                        result = new ResultElement<UsuarioDto>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<bool> CloseSession(CloseSessionParam param)   
        {
            var result = new ResultElement<bool>();
            try
            {
                #region Proceso

                var paramService = param;
                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}Session/close_session", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.POST);
                var serializeObject = JsonConvert.SerializeObject(paramService);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponseObject<bool>>(response.Content);

                        result = new ResultElement<bool>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<List<MonedaDto>> GetMoneda() 
        {
            var result = new ResultElement<List<MonedaDto>>();
            try
            {
                #region Proceso

                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}Moneda", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponse<MonedaDto>>(response.Content);

                        result = new ResultElement<List<MonedaDto>>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<TransferenciaDto> Transaccion(PostTransferenciaParam param) 
        {
            var result = new ResultElement<TransferenciaDto>();
            try
            {
                #region Proceso

                var paramService = param;
                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}Transferencia", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.POST);
                var serializeObject = JsonConvert.SerializeObject(paramService);
                request.AddHeader("content-type", "application/json");
                request.AddParameter("application/json", serializeObject, ParameterType.RequestBody);
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponseObject<TransferenciaDto>>(response.Content);

                        result = new ResultElement<TransferenciaDto>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }

        public ResultElement<List<TransferenciaTipoDto>> GetTypeTransferencia() 
        {
            var result = new ResultElement<List<TransferenciaTipoDto>>();
            try
            {
                #region Proceso

                var urlWsBase = Configuration.URL_API;
                var sUrl = string.Format("{0}transferencia_tipo", urlWsBase);
                var client = new RestClient(sUrl);
                var request = new RestRequest(Method.GET);
                request.AddHeader("content-type", "application/json");
                var response = client.Execute(request);
                var status = response.StatusCode;

                switch (status)
                {
                    case System.Net.HttpStatusCode.OK:
                        var content = JsonConvert.DeserializeObject<ApiResponse<TransferenciaTipoDto>>(response.Content);

                        result = new ResultElement<List<TransferenciaTipoDto>>()
                        {
                            codigo = content.codigo,
                            data = content.data,
                            errores = content.errores,
                            mensaje = content.mensaje
                        };
                        break;
                    default:
                        break;
                }
                #endregion
            }
            catch (Exception ex)
            {
                result.codigo = 1;
                result.mensaje = ex.Message;
            }
            return result;
        }
    }
}
