﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Service.Dtos
{
    public class MonedaDto
    {
        public short MonedaId { get; set; }
        public string Abreviacion { get; set; }
        public string Name { get; set; }
        public string Descripcion { get; set; }
        public short Estado { get; set; }
    }
}
