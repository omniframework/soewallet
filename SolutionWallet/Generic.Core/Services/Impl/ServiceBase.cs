﻿using Generic.Data.Repositories;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Generic.DataTransfer.Exceptions;

namespace Generic.Core.Services.Impl
{
    public abstract class ServiceBase : IServiceBase
    {
        public abstract void Dispose();

        public void ProcessValidations(Action<List<string>> action)
        {
            List<string> messages = new List<string>();
            action.Invoke(messages);
            if (messages.Any())
            {
                throw new ValidationException("Alerta", messages.ToArray());
            }
        }
    }

    public class ServiceBase<TRepoBase> : ServiceBase
        where TRepoBase : IRepoBase
    {
        protected TRepoBase _mainRepo;

        public ServiceBase(
            TRepoBase mainRepo)
        {
            this._mainRepo = mainRepo;
        }

        public override void Dispose()
        {
            this._mainRepo?.Dispose();
            this._mainRepo = default;
        }
    }
}
