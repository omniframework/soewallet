package com.seguridad.rest;

import com.generics.entities.ApiResponse;
import com.seguridad.entities.datatransfers.UsuarioDto;
import com.seguridad.entities.params.ChangePasswordParam;
import com.seguridad.entities.params.LoginParam;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ISessionService {
    String LOGIN = "walletapi/session/login";
    String CHANGE_PASSWORD = "walletapi/session/change_password";

    @POST(LOGIN)
    Call<ApiResponse<UsuarioDto>> login(@Body LoginParam param);

    @POST(CHANGE_PASSWORD)
    Call<ApiResponse<UsuarioDto>> change_password(@Body ChangePasswordParam param);
}
