package com.seguridad.rest.impl;

import android.content.Context;

import com.generics.entities.ApiResponse;
import com.seguridad.entities.datatransfers.UsuarioDto;
import com.seguridad.entities.params.ChangePasswordParam;
import com.seguridad.entities.params.LoginParam;
import com.seguridad.rest.ISessionService;

import comgenerics.rest.RestBase;
import retrofit2.Call;

public class ChangePasswordRest extends RestBase<UsuarioDto, ISessionService> {

    private ChangePasswordParam param;

    public ChangePasswordRest(
            Context context,
        ChangePasswordParam param) {
        super(context);
        this.param = param;
    }

    @Override
    protected Class<ISessionService> getInterfaceClass() {
        return ISessionService.class;
    }

    @Override
    protected Call<ApiResponse<UsuarioDto>> call(ISessionService interfaceRest) {
        return interfaceRest.change_password(param);
    }
}
