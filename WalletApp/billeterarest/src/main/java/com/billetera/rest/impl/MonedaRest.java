package com.billetera.rest.impl;

import android.content.Context;

import com.billetera.entidades.DataTransfers.MonedaDto;
import com.billetera.rest.IMonedaService;
import com.generics.entities.ApiResponse;

import java.util.List;

import comgenerics.rest.RestBase;
import retrofit2.Call;

public class MonedaRest extends RestBase<List<MonedaDto>, IMonedaService> {

    public MonedaRest(Context context) {
        super(context);
    }

    @Override
    protected Class<IMonedaService> getInterfaceClass() {
        return IMonedaService.class;
    }

    @Override
    protected Call<ApiResponse<List<MonedaDto>>> call(IMonedaService interfaceRest) {
        return interfaceRest.get();
    }
}
