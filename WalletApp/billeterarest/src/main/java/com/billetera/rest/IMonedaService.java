package com.billetera.rest;

import com.billetera.entidades.DataTransfers.MonedaDto;
import com.generics.entities.ApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IMonedaService {
    String MONEDA = "walletapi/moneda";

    @GET(MONEDA)
    Call<ApiResponse<List<MonedaDto>>> get();
}
