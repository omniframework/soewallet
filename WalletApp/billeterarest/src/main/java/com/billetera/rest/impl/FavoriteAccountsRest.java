package com.billetera.rest.impl;

import android.content.Context;

import com.billetera.entidades.DataTransfers.CuentaAsociadaDto;
import com.billetera.entidades.DataTransfers.CuentaDto;
import com.billetera.entidades.DataViews.GetFavoriteAccountsParam;
import com.billetera.entidades.DataViews.GetMyAccountsParam;
import com.billetera.rest.ICuentaAsociadaService;
import com.billetera.rest.ICuentaService;
import com.generics.entities.ApiResponse;

import java.util.List;

import comgenerics.rest.RestBase;
import retrofit2.Call;

public class FavoriteAccountsRest extends RestBase<List<CuentaAsociadaDto>, ICuentaAsociadaService> {

    private GetFavoriteAccountsParam param;

    public FavoriteAccountsRest(
            Context context,
            GetFavoriteAccountsParam param) {
        super(context);
        this.param = param;
    }

    @Override
    protected Class<ICuentaAsociadaService> getInterfaceClass() {
        return ICuentaAsociadaService.class;
    }

    @Override
    protected Call<ApiResponse<List<CuentaAsociadaDto>>> call(ICuentaAsociadaService interfaceRest) {
        return interfaceRest.getCuentasAsociadas(param);
    }
}
