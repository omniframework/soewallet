package com.billetera.rest.impl;
import android.content.Context;
import com.billetera.entidades.DataTransfers.CuentaDto;
import com.billetera.entidades.DataViews.GetMyAccountsParam;
import com.billetera.rest.ICuentaService;
import com.generics.entities.ApiResponse;
import java.util.List;
import comgenerics.rest.RestBase;
import retrofit2.Call;

public class MyAccountRest extends RestBase<List<CuentaDto>, ICuentaService> {

    private GetMyAccountsParam param;

    public MyAccountRest(
            Context context,
            GetMyAccountsParam param) {
        super(context);
        this.param = param;
    }

    @Override
    protected Class<ICuentaService> getInterfaceClass() {
        return ICuentaService.class;
    }

    @Override
    protected Call<ApiResponse<List<CuentaDto>>> call(ICuentaService interfaceRest) {
        return interfaceRest.getMyAccounts(param);
    }
}
