package com.billetera.rest;

import com.billetera.entidades.DataTransfers.CuentaAsociadaDto;
import com.billetera.entidades.DataViews.GetFavoriteAccountsParam;
import com.generics.entities.ApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ICuentaAsociadaService {
    String FAVORITE_ACCOUNTS = "walletapi/cuenta_asociada/favorite_account";

    @POST(FAVORITE_ACCOUNTS)
    Call<ApiResponse<List<CuentaAsociadaDto>>> getCuentasAsociadas(@Body GetFavoriteAccountsParam param);
}
