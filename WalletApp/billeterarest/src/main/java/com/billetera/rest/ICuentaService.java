package com.billetera.rest;

import com.billetera.entidades.DataTransfers.CuentaDto;
import com.billetera.entidades.DataTransfers.MonedaDto;
import com.billetera.entidades.DataViews.GetMyAccountsParam;
import com.generics.entities.ApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ICuentaService {
    String MY_ACCOUNTS = "walletapi/cuenta/my_accounts";

    @POST(MY_ACCOUNTS)
    Call<ApiResponse<List<CuentaDto>>> getMyAccounts(@Body GetMyAccountsParam param);
}
