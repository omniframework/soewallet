package comgenerics.rest;

import android.content.Context;

import com.generics.entities.ApiResponse;
import com.generics.entities.enums.ApiResponseEnum;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class RestBase<T, I> {
    protected Context context;
    private CallbackApiResponse<T> callback;

    public RestBase(Context context) {
        this.context = context;
    }

    public void call(CallbackApiResponse<T> callbackApiResponse){
        this.callback = callbackApiResponse;

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(context.getString(R.string.url_base))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Call<ApiResponse<T>> caller = call(retrofit.create(getInterfaceClass()));
        caller.enqueue(new Callback<ApiResponse<T>>() {
            @Override
            public void onResponse(Call<ApiResponse<T>> call, Response<ApiResponse<T>> response) {
                ApiResponse<T> apiResponse = response.body();
                if(apiResponse!= null){
                    if(apiResponse.getCodigo() == ApiResponseEnum.Success){
                        callback.onSucces(apiResponse);
                    }else{
                        callback.onLogicError(apiResponse);
                    }
                }else{
                    callback.onFailure(new NullPointerException("apiResponse"));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<T>> call, Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    protected abstract Class<I> getInterfaceClass();

    protected abstract Call<ApiResponse<T>> call(I interfaceRest);
}
