package comgenerics.rest;

import com.generics.entities.ApiResponse;
import com.generics.entities.enums.ApiResponseEnum;

import retrofit2.Call;

public interface CallbackApiResponse<T> {
    void onSucces(ApiResponse<T> response);
    void onLogicError(ApiResponse<T> response);
    void onFailure(Throwable t);
}
