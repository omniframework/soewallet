package com.billetera.entidades.DataViews;

public class GetMyAccountsParam {
    private long usuarioId;

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }
}
