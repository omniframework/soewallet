package com.billetera.entidades.DataViews;

public class GetFavoriteAccountsParam {
    private long usuarioId;

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }
}
