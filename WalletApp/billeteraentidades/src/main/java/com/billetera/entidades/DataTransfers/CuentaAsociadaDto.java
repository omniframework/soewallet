package com.billetera.entidades.DataTransfers;

public class CuentaAsociadaDto {
    private long cuentaAsociadaId;
    private long cuentaId;
    private long usuarioId;
    private String alias;
    private short cuentaTipoId;
    private short monedaCuentaId;
    private String cuentaTipoName;
    private String monedaCuentaAbreb;
    private String monedaCuentaName;

    public long getCuentaAsociadaId() {
        return cuentaAsociadaId;
    }

    public void setCuentaAsociadaId(long cuentaAsociadaId) {
        this.cuentaAsociadaId = cuentaAsociadaId;
    }

    public long getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(long cuentaId) {
        this.cuentaId = cuentaId;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public short getCuentaTipoId() {
        return cuentaTipoId;
    }

    public void setCuentaTipoId(short cuentaTipoId) {
        this.cuentaTipoId = cuentaTipoId;
    }

    public short getMonedaCuentaId() {
        return monedaCuentaId;
    }

    public void setMonedaCuentaId(short monedaCuentaId) {
        this.monedaCuentaId = monedaCuentaId;
    }

    public String getCuentaTipoName() {
        return cuentaTipoName;
    }

    public void setCuentaTipoName(String cuentaTipoName) {
        this.cuentaTipoName = cuentaTipoName;
    }

    public String getMonedaCuentaAbreb() {
        return monedaCuentaAbreb;
    }

    public void setMonedaCuentaAbreb(String monedaCuentaAbreb) {
        this.monedaCuentaAbreb = monedaCuentaAbreb;
    }

    public String getMonedaCuentaName() {
        return monedaCuentaName;
    }

    public void setMonedaCuentaName(String monedaCuentaName) {
        this.monedaCuentaName = monedaCuentaName;
    }
}
