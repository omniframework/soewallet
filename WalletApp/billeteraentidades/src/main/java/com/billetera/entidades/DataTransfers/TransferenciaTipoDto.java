package com.billetera.entidades.DataTransfers;

public class TransferenciaTipoDto {
    private short transferenciaTipoId;
    private String nombre;
    private String descripcion;
    private short estado;

    public short getTransferenciaTipoId() {
        return transferenciaTipoId;
    }

    public void setTransferenciaTipoId(short transferenciaTipoId) {
        this.transferenciaTipoId = transferenciaTipoId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }
}
