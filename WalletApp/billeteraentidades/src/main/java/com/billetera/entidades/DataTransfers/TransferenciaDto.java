package com.billetera.entidades.DataTransfers;

import java.util.Date;

public class TransferenciaDto {

    private long transferenciaId;
    private long transaccionOrigenId;
    private long transaccionDestinoId;
    private long cuentaOrigenId;
    private long cuentaDestinoId;
    private Date fechaOcurrencia;
    private double montoOriginal;
    private short monedaOrigenId;
    private short monedaDestinoId;
    private double tipoCambioValor;
    private double monto;
    private String concepto;
    private String descripcion;
    private short estado;
    private Date fechaAnulacion;
    private String motivoAnulacion;
    private short transferenciaTipoId;
    private String monedaOrigenAbreb;
    private String monedaOrigenName;
    private String monedaDestinoAbreb;
    private String monedaDestinoName;
    private String transferenciaTipo;

    public long getTransferenciaId() {
        return transferenciaId;
    }

    public void setTransferenciaId(long transferenciaId) {
        this.transferenciaId = transferenciaId;
    }

    public long getTransaccionOrigenId() {
        return transaccionOrigenId;
    }

    public void setTransaccionOrigenId(long transaccionOrigenId) {
        this.transaccionOrigenId = transaccionOrigenId;
    }

    public long getTransaccionDestinoId() {
        return transaccionDestinoId;
    }

    public void setTransaccionDestinoId(long transaccionDestinoId) {
        this.transaccionDestinoId = transaccionDestinoId;
    }

    public long getCuentaOrigenId() {
        return cuentaOrigenId;
    }

    public void setCuentaOrigenId(long cuentaOrigenId) {
        this.cuentaOrigenId = cuentaOrigenId;
    }

    public long getCuentaDestinoId() {
        return cuentaDestinoId;
    }

    public void setCuentaDestinoId(long cuentaDestinoId) {
        this.cuentaDestinoId = cuentaDestinoId;
    }

    public Date getFechaOcurrencia() {
        return fechaOcurrencia;
    }

    public void setFechaOcurrencia(Date fechaOcurrencia) {
        this.fechaOcurrencia = fechaOcurrencia;
    }

    public double getMontoOriginal() {
        return montoOriginal;
    }

    public void setMontoOriginal(double montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    public short getMonedaOrigenId() {
        return monedaOrigenId;
    }

    public void setMonedaOrigenId(short monedaOrigenId) {
        this.monedaOrigenId = monedaOrigenId;
    }

    public short getMonedaDestinoId() {
        return monedaDestinoId;
    }

    public void setMonedaDestinoId(short monedaDestinoId) {
        this.monedaDestinoId = monedaDestinoId;
    }

    public double getTipoCambioValor() {
        return tipoCambioValor;
    }

    public void setTipoCambioValor(double tipoCambioValor) {
        this.tipoCambioValor = tipoCambioValor;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public Date getFechaAnulacion() {
        return fechaAnulacion;
    }

    public void setFechaAnulacion(Date fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public String getMotivoAnulacion() {
        return motivoAnulacion;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public short getTransferenciaTipoId() {
        return transferenciaTipoId;
    }

    public void setTransferenciaTipoId(short transferenciaTipoId) {
        this.transferenciaTipoId = transferenciaTipoId;
    }

    public String getMonedaOrigenAbreb() {
        return monedaOrigenAbreb;
    }

    public void setMonedaOrigenAbreb(String monedaOrigenAbreb) {
        this.monedaOrigenAbreb = monedaOrigenAbreb;
    }

    public String getMonedaOrigenName() {
        return monedaOrigenName;
    }

    public void setMonedaOrigenName(String monedaOrigenName) {
        this.monedaOrigenName = monedaOrigenName;
    }

    public String getMonedaDestinoAbreb() {
        return monedaDestinoAbreb;
    }

    public void setMonedaDestinoAbreb(String monedaDestinoAbreb) {
        this.monedaDestinoAbreb = monedaDestinoAbreb;
    }

    public String getMonedaDestinoName() {
        return monedaDestinoName;
    }

    public void setMonedaDestinoName(String monedaDestinoName) {
        this.monedaDestinoName = monedaDestinoName;
    }

    public String getTransferenciaTipo() {
        return transferenciaTipo;
    }

    public void setTransferenciaTipo(String transferenciaTipo) {
        this.transferenciaTipo = transferenciaTipo;
    }
}
