package com.billetera.entidades.DataTransfers;

public class CuentaDto {
    private long cuentaId;
    private long usuarioId;
    private double balance;
    private short cuentaTipoId;
    private short monedaId;
    private short estado;
    private String cuentaTipoName;
    private String monedaAbreb;
    public String monedaName;

    public long getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(long cuentaId) {
        this.cuentaId = cuentaId;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public short getCuentaTipoId() {
        return cuentaTipoId;
    }

    public void setCuentaTipoId(short cuentaTipoId) {
        this.cuentaTipoId = cuentaTipoId;
    }

    public short getMonedaId() {
        return monedaId;
    }

    public void setMonedaId(short monedaId) {
        this.monedaId = monedaId;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public String getCuentaTipoName() {
        return cuentaTipoName;
    }

    public void setCuentaTipoName(String cuentaTipoName) {
        this.cuentaTipoName = cuentaTipoName;
    }

    public String getMonedaAbreb() {
        return monedaAbreb;
    }

    public void setMonedaAbreb(String monedaAbreb) {
        this.monedaAbreb = monedaAbreb;
    }

    public String getMonedaName() {
        return monedaName;
    }

    public void setMonedaName(String monedaName) {
        this.monedaName = monedaName;
    }
}
