package com.billetera.entidades.DataTransfers;

public class MonedaDto {
    private short monedaId;
    private String abreviacion;
    private String name;
    private String descripcion;
    private short estado;

    public short getMonedaId() {
        return monedaId;
    }

    public void setMonedaId(short monedaId) {
        this.monedaId = monedaId;
    }

    public String getAbreviacion() {
        return abreviacion;
    }

    public void setAbreviacion(String abreviacion) {
        this.abreviacion = abreviacion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }
}
