package com.generics.entities.enums;

public class StateEnum {
    public static short Disabled = 0;
    public static short Enabled = 1;
    public static short Deleted = 2;
    public static short PostVoid = 3;
}
