package com.seguridad.entities.params;

public class CloseSessionParam {
    private long usuarioId;

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }
}
