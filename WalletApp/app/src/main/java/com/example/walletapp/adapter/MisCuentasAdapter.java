package com.example.walletapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.billetera.entidades.DataTransfers.CuentaDto;
import com.example.walletapp.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

public class MisCuentasAdapter extends  RecyclerView.Adapter<MisCuentasAdapter.MiCuentaViewHolder> {
    private List<CuentaDto> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MiCuentaViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView lblCuentId;
        TextView lblBalance;
        TextView lblTipoCuenta;
        TextView lblMoneda;
        public MiCuentaViewHolder(View v) {
            super(v);
            lblCuentId = v.findViewById(R.id.lbl_cuenta_id);
            lblBalance = v.findViewById(R.id.lbl_balance);
            lblTipoCuenta = v.findViewById(R.id.lbl_cuenta_tipo);
            lblMoneda = v.findViewById(R.id.lbl_moneda);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MisCuentasAdapter(List<CuentaDto> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MisCuentasAdapter.MiCuentaViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mi_cuenta, parent, false);

        MiCuentaViewHolder vh = new MiCuentaViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MiCuentaViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        CuentaDto cuentaDto = mDataset.get(position);
        holder.lblMoneda.setText(cuentaDto.getMonedaName());
        holder.lblTipoCuenta.setText(cuentaDto.getCuentaTipoName());
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String numberAsString = decimalFormat.format(cuentaDto.getBalance());
        holder.lblBalance.setText(numberAsString);
        holder.lblCuentId.setText(cuentaDto.getCuentaId() + "");
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
