package com.example.walletapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.billetera.entidades.DataTransfers.CuentaAsociadaDto;
import com.example.walletapp.R;

import java.text.DecimalFormat;
import java.util.List;

public class CuentasAsociadasAdapter extends  RecyclerView.Adapter<CuentasAsociadasAdapter.CuentaAsociadaViewHolder> {
    private List<CuentaAsociadaDto> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class CuentaAsociadaViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView lblCuentId;
        TextView lblAlias;
        TextView lblTipoCuenta;
        TextView lblMoneda;
        public CuentaAsociadaViewHolder(View v) {
            super(v);
            lblCuentId = v.findViewById(R.id.lbl_cuenta_id);
            lblAlias = v.findViewById(R.id.lbl_alias);
            lblTipoCuenta = v.findViewById(R.id.lbl_cuenta_tipo);
            lblMoneda = v.findViewById(R.id.lbl_moneda);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CuentasAsociadasAdapter(List<CuentaAsociadaDto> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CuentaAsociadaViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cuenta_asociada, parent, false);

        CuentaAsociadaViewHolder vh = new CuentaAsociadaViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CuentaAsociadaViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        CuentaAsociadaDto cuentaAsociadaDto = mDataset.get(position);
        holder.lblMoneda.setText(cuentaAsociadaDto.getMonedaCuentaName());
        holder.lblTipoCuenta.setText(cuentaAsociadaDto.getCuentaTipoName());
        holder.lblAlias.setText(cuentaAsociadaDto.getAlias());
        holder.lblCuentId.setText(cuentaAsociadaDto.getCuentaId() + "");
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
