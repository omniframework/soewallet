package com.example.walletapp;

import android.app.Application;

import com.seguridad.entities.datatransfers.UsuarioDto;

public class GlobalApplication extends Application {

    private static UsuarioDto usuarioDto;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static UsuarioDto getUsuarioDto() {
        return usuarioDto;
    }

    public static void setUsuarioDto(UsuarioDto usuarioDto) {
        GlobalApplication.usuarioDto = usuarioDto;
    }
}
