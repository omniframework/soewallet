package com.example.walletapp.session;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.walletapp.AlertMessageBuilder;
import com.example.walletapp.GlobalApplication;
import com.example.walletapp.MainActivity;
import com.example.walletapp.R;
import com.generics.entities.ApiResponse;
import com.generics.entities.Md5Helper;
import com.seguridad.entities.datatransfers.UsuarioDto;
import com.seguridad.entities.params.LoginParam;
import com.seguridad.rest.impl.LoginRest;

import java.lang.reflect.Array;
import java.util.Arrays;

import comgenerics.rest.CallbackApiResponse;

public class LoginActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtPassword;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        txtUsuario=findViewById(R.id.txt_usuario);
        txtPassword= findViewById(R.id.txt_password);

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginParam param = new LoginParam();
                param.setNickName(txtUsuario.getText().toString().trim());

                param.setPassword(Md5Helper.md5(txtPassword.getText().toString().trim()));

                LoginRest loginRest = new LoginRest(LoginActivity.this, param);
                progressDialog.show();
                progressDialog.setMessage("Iniciando Sesión...");
                loginRest.call(new CallbackApiResponse<UsuarioDto>() {
                    @Override
                    public void onSucces(ApiResponse<UsuarioDto> response) {
                        UsuarioDto usuarioDto = response.getData();
                        GlobalApplication.setUsuarioDto(usuarioDto);
                        progressDialog.dismiss();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onLogicError(ApiResponse<UsuarioDto> response) {
                        AlertMessageBuilder.build(LoginActivity.this, response.getMensaje(), response.getErrores());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        AlertMessageBuilder.build(LoginActivity.this, "Aviso", Arrays.asList(t.getMessage()));
                        progressDialog.dismiss();
                    }
                });
            }
        });
    }
}
