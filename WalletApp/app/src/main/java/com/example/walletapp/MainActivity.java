package com.example.walletapp;

import android.content.Intent;
import android.os.Bundle;

import com.billetera.entidades.DataTransfers.CuentaAsociadaDto;
import com.billetera.entidades.DataTransfers.CuentaDto;
import com.billetera.entidades.DataViews.GetFavoriteAccountsParam;
import com.billetera.entidades.DataViews.GetMyAccountsParam;
import com.billetera.rest.impl.FavoriteAccountsRest;
import com.billetera.rest.impl.MyAccountRest;
import com.example.walletapp.adapter.CuentasAsociadasAdapter;
import com.example.walletapp.adapter.MisCuentasAdapter;
import com.example.walletapp.session.ChangePasswordActivity;
import com.example.walletapp.session.LoginActivity;
import com.generics.entities.ApiResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.core.view.GravityCompat;

import com.google.android.material.navigation.NavigationView;
import com.seguridad.entities.datatransfers.UsuarioDto;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.Menu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import comgenerics.rest.CallbackApiResponse;

public class MainActivity extends AppCompatActivity {
    private UsuarioDto usuarioDto;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    private TextView lblNombreCompleto;
    private TextView lblUser;

    private RecyclerView rvCuentasAsociadas;
    private CuentasAsociadasAdapter cuentasAsociadasAdapter;

    private RecyclerView rvMisCuentas;
    private MisCuentasAdapter misCuentasAdapter;
    private RecyclerView.LayoutManager layoutManagerCuenta;
    private RecyclerView.LayoutManager layoutManagerCuentaAsociadas;
    private SwipeRefreshLayout swipe;

    private List<CuentaDto> cuentaDtos = new ArrayList<>(0);
    private List<CuentaAsociadaDto> cuentaAsociadaDtos = new ArrayList<>(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionbar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        rvMisCuentas = findViewById(R.id.rv_cuentas_propias);
        rvCuentasAsociadas = findViewById(R.id.rv_cuentas_terceros);
        swipe = findViewById(R.id.swipe);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(usuarioDto != null){
                    loadCuentas(usuarioDto.getUsuarioId());
                }
            }
        });
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int itemId = menuItem.getItemId();
                switch (itemId) {
                    case R.id.close_session:
                        GlobalApplication.setUsuarioDto(null);
                        openLogin();
                        break;
                    case R.id.change_password:
                        openChangePassword();
                        break;
                }
                return true;
            }
        });
        rvMisCuentas.setHasFixedSize(false);
        rvCuentasAsociadas.setHasFixedSize(false);
        layoutManagerCuenta = new LinearLayoutManager(this);
        layoutManagerCuentaAsociadas = new LinearLayoutManager(this);
        rvMisCuentas.setLayoutManager(layoutManagerCuenta);
        rvCuentasAsociadas.setLayoutManager(layoutManagerCuentaAsociadas);
        misCuentasAdapter = new MisCuentasAdapter(cuentaDtos);
        cuentasAsociadasAdapter = new CuentasAsociadasAdapter(cuentaAsociadaDtos);
        rvMisCuentas.setAdapter(misCuentasAdapter);
        rvCuentasAsociadas.setAdapter(cuentasAsociadasAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.usuarioDto = GlobalApplication.getUsuarioDto();
        if (this.usuarioDto == null) {
            openLogin();
        } else {
            loadCuentas(usuarioDto.getUsuarioId());
        }
    }

    private void loadCuentas(long usuarioId) {
        GetMyAccountsParam getMyAccountsParam = new GetMyAccountsParam();
        getMyAccountsParam.setUsuarioId(usuarioId);
        MyAccountRest myAccountRest = new MyAccountRest(this, getMyAccountsParam);
        swipe.setRefreshing(true);
        myAccountRest.call(new CallbackApiResponse<List<CuentaDto>>() {
            @Override
            public void onSucces(ApiResponse<List<CuentaDto>> response) {
                cuentaDtos.clear();
                cuentaDtos.addAll(response.getData());
                misCuentasAdapter.notifyDataSetChanged();
                swipe.setRefreshing(false);
            }

            @Override
            public void onLogicError(ApiResponse<List<CuentaDto>> response) {
                AlertMessageBuilder.build(MainActivity.this, response.getMensaje(), response.getErrores());
                swipe.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                AlertMessageBuilder.build(MainActivity.this, "Aviso", Arrays.asList(t.getMessage()));
                swipe.setRefreshing(false);
            }
        });

        GetFavoriteAccountsParam getFavoriteAccountsParam = new GetFavoriteAccountsParam();
        getFavoriteAccountsParam.setUsuarioId(usuarioId);
        FavoriteAccountsRest favoriteAccountRest = new FavoriteAccountsRest(this, getFavoriteAccountsParam);
        swipe.setRefreshing(true);
        favoriteAccountRest.call(new CallbackApiResponse<List<CuentaAsociadaDto>>() {
            @Override
            public void onSucces(ApiResponse<List<CuentaAsociadaDto>> response) {
                cuentaAsociadaDtos.clear();
                cuentaAsociadaDtos.addAll(response.getData());
                cuentasAsociadasAdapter.notifyDataSetChanged();
                swipe.setRefreshing(false);
            }

            @Override
            public void onLogicError(ApiResponse<List<CuentaAsociadaDto>> response) {
                AlertMessageBuilder.build(MainActivity.this, response.getMensaje(), response.getErrores());
                swipe.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                AlertMessageBuilder.build(MainActivity.this, "Aviso", Arrays.asList(t.getMessage()));
                swipe.setRefreshing(false);
            }
        });
    }


    private void loadUsuarioInfo() {

    }

    private void openChangePassword() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
        finish();
    }

    private void openLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            // Android home
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return true;
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        MonedaRest monedaRest = new MonedaRest(this);
//        monedaRest.call(new CallbackApiResponse<List<MonedaDto>>() {
//            @Override
//            public void onSucces(ApiResponse<List<MonedaDto>> response) {
//                Toast.makeText(MainActivity.this, response.getData().size()+"", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onLogicError(ApiResponse<List<MonedaDto>> response) {
//                Toast.makeText(MainActivity.this, response.getMensaje(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        GetMyAccountsParam getMyAccountsParam = new GetMyAccountsParam();
//        getMyAccountsParam.setUsuarioId(4);
//
//        MyAccountRest myAccountRest = new MyAccountRest(this, getMyAccountsParam);
//        myAccountRest.call(new CallbackApiResponse<List<CuentaDto>>() {
//            @Override
//            public void onSucces(ApiResponse<List<CuentaDto>> response) {
//                Toast.makeText(MainActivity.this, response.getData().size() +"", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onLogicError(ApiResponse<List<CuentaDto>> response) {
//                Toast.makeText(MainActivity.this, response.getMensaje(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Throwable t) {
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

}
