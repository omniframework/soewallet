package com.example.walletapp.session;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.walletapp.AlertMessageBuilder;
import com.example.walletapp.GlobalApplication;
import com.example.walletapp.MainActivity;
import com.example.walletapp.R;
import com.generics.entities.ApiResponse;
import com.generics.entities.Md5Helper;
import com.seguridad.entities.datatransfers.UsuarioDto;
import com.seguridad.entities.params.ChangePasswordParam;
import com.seguridad.entities.params.LoginParam;
import com.seguridad.rest.impl.ChangePasswordRest;
import com.seguridad.rest.impl.LoginRest;

import java.util.Arrays;

import comgenerics.rest.CallbackApiResponse;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText txtPassword;
    private EditText txtNewPassword;
    private EditText txtConfirmNewPassword;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        txtNewPassword=findViewById(R.id.txt_new_password);
        txtConfirmNewPassword=findViewById(R.id.txt_confirm_new_password);
        txtPassword= findViewById(R.id.txt_password);

        findViewById(R.id.btn_change_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtPassword.getText().toString().trim().equals("")){
                    AlertMessageBuilder.build(ChangePasswordActivity.this, "Aviso", Arrays.asList("El campo \"Contraseña\" es requerido."));
                    return;
                }
                if(txtNewPassword.getText().toString().trim().equals("")){
                    AlertMessageBuilder.build(ChangePasswordActivity.this, "Aviso", Arrays.asList("El campo \"Nueva Contraseña\" es requerido."));
                    return;
                }
                if(txtConfirmNewPassword.getText().toString().trim().equals(txtNewPassword.getText().toString().trim())){
                    ChangePasswordParam param = new ChangePasswordParam();
                    param.setPassword(Md5Helper.md5(txtPassword.getText().toString().trim()));
                    param.setNewPassword(Md5Helper.md5(txtNewPassword.getText().toString().trim()));
                    UsuarioDto usuarioDto = GlobalApplication.getUsuarioDto();
                    param.setUsuarioId(usuarioDto.getUsuarioId());

                    ChangePasswordRest loginRest = new ChangePasswordRest(ChangePasswordActivity.this, param);
                    progressDialog.show();
                    progressDialog.setMessage("Cambiando Contraseña...");
                    loginRest.call(new CallbackApiResponse<UsuarioDto>() {
                        @Override
                        public void onSucces(ApiResponse<UsuarioDto> response) {
                            UsuarioDto usuarioDto = response.getData();
                            GlobalApplication.setUsuarioDto(usuarioDto);
                            progressDialog.dismiss();
                            Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onLogicError(ApiResponse<UsuarioDto> response) {
                            AlertMessageBuilder.build(ChangePasswordActivity.this, response.getMensaje(), response.getErrores());
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            AlertMessageBuilder.build(ChangePasswordActivity.this, "Aviso", Arrays.asList(t.getMessage()));
                            progressDialog.dismiss();
                        }
                    });
                }else{
                    AlertMessageBuilder.build(ChangePasswordActivity.this, "Aviso", Arrays.asList("La nueva contraseña y confirmaciòn de nueva contraseña deben coincidir"));
                }
            }
        });
    }
}
